//
//  CreateNewGuestVC.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/18/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import ContactsUI
import AddressBook

class AddNewGuestVC: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,CNContactPickerDelegate {
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var addFromContactsBtn: UIButton!
    var currentGuestname = ""
    @IBOutlet weak var guestName: UITextField!
    @IBOutlet weak var guestStreet: UITextField!
    @IBOutlet weak var guestCity: UITextField!
    @IBOutlet weak var guestState: UITextField!
    @IBOutlet weak var guestZipCode: UITextField!
    @IBOutlet weak var guestEmailAddress: UITextField!
    @IBOutlet weak var guestPhoneNum: UITextField!
    @IBOutlet weak var assignGuestToParty: UIButton!
    @IBOutlet weak var assignRSVPStatus: UIButton!
    @IBOutlet weak var guestCount: UITextField!
    
    var guestPartyPicker: UIPickerView = UIPickerView()
    var guestParty : NSArray =  ["Groom's Guest", "Bride's Guest"]
    
    var rsvpPicker: UIPickerView = UIPickerView()
    var rsvp : NSArray =  ["Regret", "Accept"]
    
    var contactStore = CNContactStore()
    let peoplePicker = CNContactPickerViewController()
    var userAccessGranted : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.guestPartyPicker.delegate = self
        self.rsvpPicker.delegate = self
        configurePicker()
        
        self.checkIfUserAccessGranted()
        peoplePicker.delegate = self
        
        guestName.delegate = self
        guestStreet.delegate = self
        guestCity.delegate = self
        guestState.delegate = self
        guestZipCode.delegate = self
        guestPhoneNum.delegate = self
        guestEmailAddress.delegate = self
        guestCount.delegate = self
        
        assignGuestToParty.layer.cornerRadius = self.assignGuestToParty.frame.size.height/3
        assignGuestToParty.clipsToBounds = true
        
        assignRSVPStatus.layer.cornerRadius = self.assignRSVPStatus.frame.size.height/3
        assignRSVPStatus.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification:Notification) {
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        topConstraint.constant = keyboardHeight - 260.0
        self.updateView()
    }
    
    func keyboardWillHide(_ notification:Notification) {
        topConstraint.constant = 12.0
        self.updateView()
    }
    
    fileprivate func updateView() {
        UIView.animate(withDuration: 0.24, animations: {
            self.addFromContactsBtn.alpha = self.topConstraint.constant == 12.0 ? 1:0
            self.view.layoutIfNeeded()
        }) 
    }
    
    func configurePicker(){
        self.guestPartyPicker.dataSource = self
        self.guestPartyPicker.frame = CGRect(x: 0, y: 340, width: self.view.frame.width, height: 100)
        self.guestPartyPicker.backgroundColor = iAdornConstants.fushiaColor
        self.guestPartyPicker.tintColor = UIColor.white
        
        self.rsvpPicker.dataSource = self
        self.rsvpPicker.frame = CGRect(x: 0, y: 340, width: self.view.frame.width, height: 100)
        self.rsvpPicker.backgroundColor = iAdornConstants.fushiaColor
        self.rsvpPicker.tintColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveGuest(_ sender: AnyObject) {
        
        let guestFirstName = self.guestName.text
        var guestCnt = 0
        if self.guestCount.text! != ""{
            guestCnt = Int(self.guestCount.text!)!
        }
        
        var rsvp = ""
        if self.assignRSVPStatus.titleLabel!.text! == "RSVP Pending"{
            rsvp = "Regret"
        }else{
            rsvp = self.assignRSVPStatus.titleLabel!.text!
        }
        
        if guestFirstName != "" {
            
            // Build the new Guest.
            // AnyObject is needed because of the guestFamilySize of type Int.
            let newGuest: Dictionary<String, AnyObject> = [
                "guestname": guestFirstName! as AnyObject,
                "guestAddress_Street": self.guestStreet.text! as AnyObject,
                "guestAddress_City": self.guestCity.text! as AnyObject,
                "guestAddress_State": self.guestState.text! as AnyObject,
                "guestAddress_ZipCode": self.guestZipCode.text! as AnyObject,
                "guestEmailAddress": self.guestEmailAddress.text! as AnyObject,
                "guestPhoneNumber": self.guestPhoneNum.text! as AnyObject,
                "guestFamilySize": guestCnt as AnyObject,
                "guestRSVPStatus": rsvp as AnyObject,
                "assignGuestToParty": (self.assignGuestToParty.titleLabel?.text)! as AnyObject
            ]
            // Send it over to DataService to seal the deal.
            //print(newGuest)
            NetworkManager.sharedInstance.createNewGuest(newGuest)
            
            
            let date = Date()
            // create a new feed for the home scene.
            let newHomeFeed: Dictionary<String, AnyObject> = [
                "_feedMessage": "\(guestFirstName!) was added as a guest" as AnyObject,
                "_feedDate": date.formatAsStringWithTime as AnyObject
            ]
            // Send it over to DataService to seal the deal.
            //print(newHomeFeed)
            NetworkManager.sharedInstance.createHomeFeed(newHomeFeed)
            
            self.dismiss(animated: true, completion: nil)
        }else{
            showAlertWithOKButton("Warning!", message: "Guest Name Is Requried", presentingViewController: self)
        }
    }
    
    
    @IBAction func addFromUsersContacts(_ sender: UIButton) {
        pickContacts()
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func assignGuestToParty(_ sender: UIButton) {
        self.view.addSubview(self.guestPartyPicker)
    }
    
    @IBAction func assignRSVPStatus(_ sender: UIButton) {
        self.view.addSubview(self.rsvpPicker)
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == guestPartyPicker{
            return guestParty.count
        }else {
            return rsvp.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == guestPartyPicker{
            return guestParty[row] as? String
        }else {
            return rsvp[row] as? String
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == guestPartyPicker{
            //print(guestParty[row] as? String)
            let result = guestParty[row] as? String
            assignGuestToParty.setTitle(result, for: UIControlState())
            guestPartyPicker.removeFromSuperview()
        }else {
            //print(rsvp[row] as? String)
            let result = rsvp[row] as? String
            assignRSVPStatus.setTitle(result, for: UIControlState())
            rsvpPicker.removeFromSuperview()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if pickerView == guestPartyPicker{
            let attributedString = NSAttributedString(string: guestParty[row] as! String, attributes: [NSForegroundColorAttributeName : UIColor.white])
            return attributedString
        }else {
            let attributedString = NSAttributedString(string: rsvp[row] as! String, attributes: [NSForegroundColorAttributeName : UIColor.white])
            return attributedString
        }
    }
    
    // MARK: - CNContactPickerViewController Delegate
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        //Dismiss the peoplePicker
        peoplePicker.dismiss(animated: true, completion: nil)
        
        //See if the contact has multiple phone numbers
        if contact.phoneNumbers.count > 1 {
            
            //If so we need the user to select which phone number we want them to use
            let multiplePhoneNumbersAlert = UIAlertController(title: "Select A Phone Number", message: "This contact has multiple phone numbers, which one would you like to use?", preferredStyle: UIAlertControllerStyle.alert)
            //Loop through all the phone numbers that we got back
            for number in contact.phoneNumbers {
                //Each object in the phone numbers array has a value property that is a CNPhoneNumber object, Make sure we can get that
                if let actualNumber = number.value as? CNPhoneNumber {
//                    print(actualNumber.stringValue)
                    
                    //Get the label for the phone number
                    var phoneNumberLabel = number.label
                    //Strip off all the extra crap that comes through in that label
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "_", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "!", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil)
                    //Create a title for the action for the UIAlertVC that we display to the user to pick phone numbers
                    let actionTitle = phoneNumberLabel! + " : " + actualNumber.stringValue
                    //Create the alert action
                    let numberAction = UIAlertAction(title: actionTitle, style: UIAlertActionStyle.default, handler: { (theAction) -> Void in
                        //Create an empty string for the contacts name
                        var nameToSave = ""
                        //See if we can get frist name
                        if contact.givenName == "" {
                            //If "" check for a last name
                            if contact.familyName != "" {
                                nameToSave = contact.familyName
                            }
                        }else{
                            if contact.familyName != "" {
                                nameToSave = contact.givenName + contact.familyName
                            }else{
                                nameToSave = contact.givenName
                            }
                        }
                        self.guestName.text! = nameToSave
                        
                        //WORK IN PROGRESS - SAVING IMAGE NOT CURRENTLY IMPLEMENTED
                        /*
                         // See if we can get image data
                         if let imageData = contact.imageData {
                         //If so create the image
                         let userImage = UIImage(data: imageData)
                         //  self.guestImage.image = userImage;
                         }
                         */
                        
                        //Get the string value of the phone number like this:
                        actualNumber.stringValue
                       // print(actualNumber.stringValue)
                        self.guestPhoneNum.text! = actualNumber.stringValue
                        if let emailFirst = contact.emailAddresses.first?.value  {
                            self.guestEmailAddress.text! = emailFirst as String
                        }
                        
                        if contact.postalAddresses.count > 0{
                            if let address = contact.postalAddresses.first!.value as? CNPostalAddress {
                                 if address.street != ""{
                                    self.guestStreet.text! = address.street
                                }else{
                                    // if street value is empty
                                    self.guestStreet.placeholder = "Street"
                                    self.guestStreet.text! = ""
                                }
                                
                                if address.city != ""{
                                    self.guestCity.text! = address.city
                                }else{
                                    // if city value is empty
                                    self.guestCity.placeholder = "City"
                                    self.guestCity.text! = ""
                                }
                                
                                if address.state != ""{
                                    self.guestState.text! = address.state
                                }else{
                                    // if state value is empty
                                    self.guestState.placeholder = "State"
                                    self.guestState.text! = ""
                                }
                                
                                if address.postalCode != ""{
                                    self.guestZipCode.text! = address.postalCode
                                }else{
                                    // if postal code value is empty
                                    self.guestZipCode.placeholder = "Postal Code"
                                    self.guestZipCode.text! = ""
                                }
                            }
                        } else{
                            //in case user adds from contact a second time and chooses a different guest
                            self.guestStreet.placeholder = "Street"
                            self.guestStreet.text! = ""
                            self.guestCity.placeholder = "City"
                            self.guestCity.text! = ""
                            self.guestState.placeholder = "State"
                            self.guestState.text! = ""
                            self.guestZipCode.placeholder = "Postal Code"
                            self.guestZipCode.text! = ""
                        }
                        
                    })
                    //Add the action to the AlertController
                    multiplePhoneNumbersAlert.addAction(numberAction)
                }
            }
            //Add a cancel action
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (theAction) -> Void in
              })
            //Add the cancel action
            multiplePhoneNumbersAlert.addAction(cancelAction)
            //Present the ALert controller
            self.present(multiplePhoneNumbersAlert, animated: true, completion: nil)
        }else{
            //Make sure we have at least one phone number
            if contact.phoneNumbers.count > 0 {
                
                //If so get the CNPhoneNumber object from the first item in the array of phone numbers
                if let actualNumber = contact.phoneNumbers.first!.value as? CNPhoneNumber {
                    //Get the label of the phone number
                    var phoneNumberLabel = contact.phoneNumbers.first!.label
                    //Strip out the stuff you don't need
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "_", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel =
                        phoneNumberLabel?.replacingOccurrences(of: "!", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil)
                    
                    //Create an empty string for the contacts name
                    var nameToSave = ""
                    //See if we can get A frist name
                    if contact.givenName == "" {
                        //If "" check for a last name
                        if contact.familyName != "" {
                            nameToSave = contact.familyName
                        }
                    }else{
                        if contact.familyName != "" {
                            nameToSave = contact.givenName + contact.familyName
                        }else{
                            nameToSave = contact.givenName
                        }
                    }
                     self.guestName.text! = nameToSave
                    
                    //WORK IN PROGRESS - SAVING IMAGE NOT CURRENTLY IMPLEMENTED
                    /*
                     // See if we can get image data
                     if let imageData = contact.imageData {
                     //If so create the image
                     let userImage = UIImage(data: imageData)
                     // self.guestImage.image = userImage;
                     }
                     */
                    
                    //Do what you need to do with your new contact information here!
                    //print(actualNumber.stringValue)
                    if actualNumber.stringValue != ""{
                        self.guestPhoneNum.text! = actualNumber.stringValue
                    }else{
                        self.guestPhoneNum.text! = "Phone"
                        self.guestPhoneNum.placeholder = "Phone"
                        
                    }
                    
                    
                    if let emailFirst = contact.emailAddresses.first?.value  {
                         self.guestEmailAddress.text! = emailFirst as String
                    }else{
                        self.guestEmailAddress.text! = ""
                        self.guestEmailAddress.placeholder = "Email"
                    }
                    
                    if contact.postalAddresses.count > 0{
                        if let address = contact.postalAddresses.first!.value as? CNPostalAddress {
                             if address.street != ""{
                                self.guestStreet.text! = address.street
                            }else{
                                // if street value is empty
                                self.guestStreet.placeholder = "Street"
                                self.guestStreet.text! = ""
                            }
                            
                            if address.city != ""{
                                self.guestCity.text! = address.city
                            }else{
                                // if city value is empty
                                self.guestCity.placeholder = "City"
                                self.guestCity.text! = ""
                            }
                            
                            if address.state != ""{
                                self.guestState.text! = address.state
                            }else{
                                // if state value is empty
                                self.guestState.placeholder = "State"
                                self.guestState.text! = ""
                            }
                            
                            if address.postalCode != ""{
                                self.guestZipCode.text! = address.postalCode
                            }else{
                                // if postal code value is empty
                                self.guestZipCode.placeholder = "Postal Code"
                                self.guestZipCode.text! = ""
                            }
                        }
                    } else{
                        //in case user adds from contact a second time and chooses a different guest
                        self.guestStreet.placeholder = "Street"
                        self.guestStreet.text! = ""
                        self.guestCity.placeholder = "City"
                        self.guestCity.text! = ""
                        self.guestState.placeholder = "State"
                        self.guestState.text! = ""
                        self.guestZipCode.placeholder = "Postal Code"
                        self.guestZipCode.text! = ""
                    }
                    
                }
            }
        }
    }
    
    //Contacts
    func pickContacts() {
        self.present(peoplePicker, animated: true, completion: nil)
    }
    
    func requestForAccess(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
            
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let message = "\(accessError!.localizedDescription)\n\nPlease allow the app to access your contacts through the Settings."
                            showAlertWithOKButton("OK", message: message, presentingViewController: self)
                        })
                    }
                }
            })
            
        default:
            completionHandler(false)
        }
    }
    
    func checkIfUserAccessGranted()
    {
        self.requestForAccess { (accessGranted) -> Void in
            if accessGranted {
                self.userAccessGranted = true;
            }else{
                self.userAccessGranted = false;
            }
        }
    }
    
    
}
