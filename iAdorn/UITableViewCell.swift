//
//  UITableViewCell.swift
//  iAdorn
//
//  Created by Jaxs on 5/23/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import Foundation

import UIKit

extension UITableViewCell {
    var parentTableView: UITableView? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let tableView = parentResponder as? UITableView {
                return tableView
            }
        }
        return nil
    }
    
    var parentViewController: UIViewController? {
        return self.parentTableView?.delegate as? UIViewController
    }
}
