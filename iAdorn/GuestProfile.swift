//
//  DBManager.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/16/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class GuestProfile: NSObject {
    
    fileprivate var _guestRef: FIRDatabaseReference!
    fileprivate var _guestName: String!
    fileprivate var _guestKey: String!
    fileprivate var _guestAddress_Street: String!
    fileprivate var _guestAddress_City: String!
    fileprivate var _guestAddress_State: String!
    fileprivate var _guestAddress_ZipCode: String!
    fileprivate var _guestAddress_Country: String!
    fileprivate var _guestEmailAddress: String!
    fileprivate var _guestPhoneNumber: String!
    fileprivate var _guestFamilySize: Int!
    fileprivate var _guestRSVPStatus: String!
    fileprivate var _assignGuestToParty: String!
    
    var guestKey: String {
        return _guestKey
    }
    
    var guestName: String {
        return _guestName
    }
    
    var guestAddress_Street: String {
        return _guestAddress_Street
    }
    
    var guestAddress_City: String {
        return _guestAddress_City
    }
    
    var guestAddress_State: String {
        return _guestAddress_State
    }
    
    var guestAddress_ZipCode: String {
        return _guestAddress_ZipCode
    }
    
    var guestAddress_Country: String {
        return _guestAddress_Country
    }
    
    var guestEmailAddress: String {
        return _guestEmailAddress
    }
    
    var guestPhoneNumber: String {
        return _guestPhoneNumber
    }
    
    var guestFamilySize: Int {
        return _guestFamilySize
    }
    
    var guestRSVPStatus: String {
        return _guestRSVPStatus
    }
    
    var assignGuestToParty: String {
        return _assignGuestToParty
    }
    
    // Initialize the new Guest
    init(key: String, dictionary: Dictionary<String, AnyObject>) {
        self._guestKey = key
        
        // Within the Guest, or Key, the following properties are children
        
        if let guest = dictionary["guestname"] as? String {
            self._guestName = guest
        } else {
            self._guestName = ""
        }
        
        if let guestAddress_Street = dictionary["guestAddress_Street"] as? String {
            self._guestAddress_Street = guestAddress_Street
        }
        
        if let guestAddress_City = dictionary["guestAddress_City"] as? String {
            self._guestAddress_City = guestAddress_City
        }
        
        if let guestAddress_State = dictionary["guestAddress_State"] as? String {
            self._guestAddress_State = guestAddress_State
        }
        
        if let guestAddress_ZipCode = dictionary["guestAddress_ZipCode"] as? String {
            self._guestAddress_ZipCode = guestAddress_ZipCode
        }
        
        if let guestEmailAddress = dictionary["guestEmailAddress"] as? String {
            self._guestEmailAddress = guestEmailAddress
        }
        
        if let guestPhoneNumber = dictionary["guestPhoneNumber"] as? String {
            self._guestPhoneNumber = guestPhoneNumber
        }
        
        if let guestFamilySize = dictionary["guestFamilySize"] as? Int {
            self._guestFamilySize = guestFamilySize
        }else{
            self._guestFamilySize = 0
        }
        
        if let guestRSVPStatus = dictionary["guestRSVPStatus"] as? String {
            self._guestRSVPStatus = guestRSVPStatus
        }else{
            self._guestRSVPStatus = "Pending"
        }
        
        if let assignGuestToParty = dictionary["assignGuestToParty"] as? String {
            self._assignGuestToParty = assignGuestToParty
        }else{
            self._assignGuestToParty = "Groom's Guest"
        }
        
        self._guestRef = NetworkManager.sharedInstance.GUEST_REF.child(self._guestKey)

    }

    
    func updatRSVPStatus(_ guestName: String, update: Bool) {
        
        if update {
            _guestRSVPStatus = "Accept"
        } else {
            _guestRSVPStatus = "Regret"
        }
        
        // Save the update in Firebase
        _guestRef.child("guestRSVPStatus").setValue(_guestRSVPStatus)
        
        //update Home Feed
        let date = Date()
        // create a new feed for the home scene.
        let newHomeFeed: Dictionary<String, AnyObject> = [
            "_feedMessage": "\(guestName)'s RSVP status changed to \"\(_guestRSVPStatus! )\"" as AnyObject,
            "_feedDate": date.formatAsStringWithTime as AnyObject
        ]
        // Send it over to DataService to seal the deal.
        //print(newHomeFeed)
        NetworkManager.sharedInstance.createHomeFeed(newHomeFeed)
        
    }
}
