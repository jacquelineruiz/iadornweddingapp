//
//  LeftMenu.swift
//  iAdorn
//
//  Created by Jaxs on 4/20/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//
import Foundation
import UIKit
import SwiftLoader
import FirebaseAuth

enum LeftSideMenu: Int {
    case main = 0
    case inspirationBoard
    case guestList
    case checkList
}

class LeftMenu: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var meTitle: UILabel!
    @IBOutlet weak var meSubtitle: UILabel!
    
    @IBOutlet weak var SideMenuTableView: UITableView!
    var displaySwiftLoader : Bool = true
    var conditionsForDisplay : Int = 0

    var selectedCellIndexPath: IndexPath! = IndexPath(row: 0, section: 0)
    
    var menuOptions = ["Home", "Inspiration", "Guest List", "Check List", "Log out"]
    var storyboardIDs = ["Home", "InspirationBoard", "GuestList", "CheckList", ""]
    var navigationControllerIDs = ["Home", "InspirationBoard", "InitialGuestListVC", "CheckList", ""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topView.isHidden = true
        self.SideMenuTableView.isHidden = true
        displaySwiftLoader = true
        
        SideMenuTableView.tableFooterView = UIView()
        SideMenuTableView.separatorColor = UIColor.clear
        
        updateUserInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.conditionsForDisplay = 0
         SideMenuTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.conditionsForDisplay = self.conditionsForDisplay + 1
        if self.conditionsForDisplay >= 1 {
            self.view.backgroundColor = iAdornConstants.fushiaColor

            self.topView.isHidden = false
            self.SideMenuTableView.isHidden = false
            self.conditionsForDisplay = 1
        }
    }
    
    internal var isVisible: Bool {
        if isViewLoaded {
            return view.window != nil
        }
        return false
    }
    
    internal var isTopViewController: Bool {
        if self.navigationController != nil {
            return self.navigationController?.visibleViewController === self
        } else if self.tabBarController != nil {
            return self.tabBarController?.selectedViewController == self && self.presentedViewController == nil
        } else {
            return self.presentedViewController == nil && self.isVisible
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUserInfo(){
        SwiftLoader.show(animated: true)
        NetworkManager.sharedInstance.CURRENT_USER_REF.observe(.value, with: { snapshot in
            let name = (snapshot.value! as AnyObject).object(forKey: "username") as! String
                //snapshot.value!["username"]
            self.meTitle.text = name 
            
            }, withCancel: { (error) -> Void in
                print(error.localizedDescription)
        })
        SwiftLoader.hide()
    }
    
    @IBAction func showMe(_ sender: UIButton) {
        
        let storyboard:UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "InitialProfile") as! Profile
        self.slideMenuController()?.changeMainViewController(vc, close: true)
        
        //Highlight "Home" as selectedCell
        selectedCellIndexPath = IndexPath(row: 0, section: 0)
    }

    //MARK: table view data source and delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LeftMenuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! LeftMenuCell
        cell.selectedIndicatorView.isHidden = true

        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.cellLabel.text = menuOptions[(indexPath as NSIndexPath).row]
        
        if selectedCellIndexPath != indexPath
        {
            cell.cellLabel?.font = UIFont(name: "GeezaPro", size: 15)
        }else{
            cell.cellLabel?.font = UIFont(name: "GeezaPro-Bold", size: 20)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath != selectedCellIndexPath)
        {
            let selectedCell: LeftMenuCell = tableView .cellForRow(at: selectedCellIndexPath)! as! LeftMenuCell
            selectedCell.cellLabel?.font = UIFont(name: "GeezaPro", size: 15)
            
            let cell: LeftMenuCell = tableView .cellForRow(at: indexPath)! as! LeftMenuCell
            cell.cellLabel?.font = UIFont(name: "GeezaPro-Bold", size: 20)
            selectedCellIndexPath = indexPath
        }
        
        if((indexPath as NSIndexPath).row == menuOptions.count - 1) {
            // logout
            SwiftLoader.show(animated: true)
            // unauth() is the logout method for the current user.
            try! FIRAuth.auth()!.signOut()
            
            // Remove the user's uid from storage.
            UserDefaults.standard.setValue(nil, forKey: "uid")
            
            // Head back to Login!
           self.slideMenuController()?.dismiss(animated: true, completion: nil)
            SwiftLoader.hide()
            return
        }
        
        if let menu = LeftSideMenu(rawValue: (indexPath as NSIndexPath).item) {
            self.changeViewController(menu)
        }
    }
    
    func changeViewController(_ menu: LeftSideMenu) {
        switch menu {
        case .main:
            let storyboard:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        case .inspirationBoard:
            let storyboard:UIStoryboard = UIStoryboard(name: "InspirationBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "InspirationBoard") as! InitialInspirationBoard
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        case .guestList:
            let storyboard:UIStoryboard = UIStoryboard(name: "GuestList", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "InitialGuestListVC") as! InitialGuestListVC
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        case .checkList:
            let storyboard:UIStoryboard = UIStoryboard(name: "CheckList", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CheckList") as! InitialCheckList
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        }
    }
}
