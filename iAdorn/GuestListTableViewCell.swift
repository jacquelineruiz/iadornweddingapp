//
//  GuestListTableViewCell.swift
//  iAdorn
//
//  Created by Jaxs on 5/13/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import SwiftLoader

class GuestListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var numOfGuest: UILabel!
    @IBOutlet weak var rsvpStatus: UILabel!
    
    var guest: GuestProfile!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(GuestListTableViewCell.rsvpTapped(_:)))
        tap.numberOfTapsRequired = 1
        rsvpStatus.addGestureRecognizer(tap)
        rsvpStatus.isUserInteractionEnabled = true
        
        rsvpStatus.layer.cornerRadius = self.rsvpStatus.frame.size.height/3
        rsvpStatus.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ guest: GuestProfile) {
        self.guest = guest
        self.name.text = "\(guest.guestName)"
        self.rsvpStatus.text = "\(guest.guestRSVPStatus)"
        self.numOfGuest.text = "\(guest.guestFamilySize)"
        let key = self.guest.guestKey
        
        NetworkManager.sharedInstance.Current_User_Guests.child(key).observeSingleEvent(of: .value
            , with: { snapshot in
                
                let status = (snapshot.value! as AnyObject).object(forKey: "guestRSVPStatus") as! String
                if (status == "Regret"){
                    // user sets current guest as regretting the invite
                    self.rsvpStatus.text = "Regret"
                    self.rsvpStatus.backgroundColor = UIColor.lightGray
                    self.rsvpStatus.textColor = iAdornConstants.fushiaColor
                    
                } else {
                    // user sets current guest as accepting the invite
                    self.rsvpStatus.text = "Accept"
                    self.rsvpStatus.backgroundColor = iAdornConstants.fushiaColor
                    self.rsvpStatus.textColor = UIColor.white
                }
            }, withCancel: { (error) -> Void in
                print(error.localizedDescription)
        })
    }
    
    func rsvpTapped(_ sender: UITapGestureRecognizer) {
        let key = self.guest.guestKey
        let name = self.guest.guestName
        
        let alert_rsvp = UIAlertController(title: "RSVP Status", message: "Are you sure you would like to change the RSVP status?", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction) -> Void in
            SwiftLoader.show(animated: true)
            NetworkManager.sharedInstance.Current_User_Guests.child(key).observeSingleEvent(of: .value
                , with: { snapshot in
                    
                    let status = (snapshot.value! as AnyObject).object(forKey: "guestRSVPStatus") as! String
                    if (status == "Regret") {
                        //updatRSVPStatus(), in GuestProfile.swift, handles the update.
                        self.guest.updatRSVPStatus(name, update: true)
                    } else {
                        //updatRSVPStatus(), in GuestProfile.swift, handles the update.
                        self.guest.updatRSVPStatus(name, update: false)
                    }
                    SwiftLoader.hide()
            })
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert_rsvp.addAction(okAction)
        alert_rsvp.addAction(cancelAction)
         self.parentViewController?.present(alert_rsvp, animated: true, completion: nil)
    }
}
