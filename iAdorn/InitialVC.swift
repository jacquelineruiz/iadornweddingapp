//
//  InitialVC.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/16/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import FirebaseAuth

class InitialVC: UIViewController {

    @IBOutlet weak var iadornTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //I ADDED A BORDER TO TITLE SINCE IT WAS HARD TO VIEW
        let title = "iAdorn"
        let stringLength = NSString(string: title).length
        let strokeString = NSMutableAttributedString(string: title, attributes: [NSFontAttributeName:UIFont(name: "GeezaPro-Bold", size: 60.0)!])
        strokeString.addAttribute(NSForegroundColorAttributeName, value: iAdornConstants.fushiaColor, range: NSRange(location:0,length: stringLength))
        strokeString.addAttribute(NSStrokeColorAttributeName, value: UIColor.black, range:  NSRange(location: 0, length: stringLength))
        strokeString.addAttribute(NSStrokeWidthAttributeName, value: -1, range: NSRange(location: 0, length: stringLength))
        self.iadornTitle.attributedText = strokeString
        
        checkIfUserIsAlreadySignedIn()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkIfUserIsAlreadySignedIn() {
        
            if let uid = UserDefaults.standard.value(forKey: "uid")  {
                print(uid)
                // User is signed in.
                let storyboard:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "ContainerSlideMenu") as? ContainerSlideMenu
                    else{return}
                self.navigationController?.pushViewController(vc, animated: true)
            }
    }
    
    @IBAction func signupAction(_ sender: AnyObject) {
        guard let vc = self.storyboard!.instantiateViewController(withIdentifier: "Signup") as? SignupVC
        else{return}
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func loginAction(_ sender: AnyObject) {
        goToLoginFirstVC()
    }
    
    func goToLoginFirstVC(){
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "LoginFirstVC") as? LoginVC
            else{return;}
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToInitialVC() {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "InitialVC") as? InitialVC
            else{return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
