//
//  String.swift
//  iAdorn
//
//  Created by Jaxs on 6/25/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import Foundation
import UIKit

extension String{
    
    var formatAsDate: Date {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter.date(from: self)!
    }

    var formatAsDateWithTime: Date {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .short
        return formatter.date(from: self)!
    }
    
    /*
    func isValidPhone() -> Bool {
        guard let text:String = self else {return true}
        if text.isEmptyOrWhitespace()
        {
            return false
        }
        
        let charcter  = CharacterSet(charactersIn: "0123456789").inverted
        var filtered:NSString!
        
        
        let inputString = text.components(separatedBy: charcter).filter{$0.isEmpty}
        
        //let inputString:NSArray = text.components(separatedBy: charcter)
        //filtered = inputString.componentsJoined(by: "") as NSString!
        if(text.characters.count != 10) {
            return false
        }
        
        //return  text == filtered as String
        return inputString
    }
    
    func isValidEmail() -> Bool {
        guard let text:String = self else {return true}
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = text.range(of: emailRegEx, options:.regularExpression)
        let result = range != nil ? true : false
        return result
    }
    
 */
    func isEmptyOrWhitespace() -> Bool {
        
        if(self.isEmpty)
        {
            return true
        }
        
        return (self.trimmingCharacters(in: CharacterSet.whitespaces) == "")
    }
    
    mutating func formatAsPhone(){
        self.unFormatPhone()
 
        if self.characters.count > 0 {
            self = "(\(self)"
        }
        
        
        if self.characters.count > 4 {
            self.insert(")", at:self.characters.index(self.startIndex, offsetBy: 4))
            self.insert("-", at:self.characters.index(self.startIndex, offsetBy: 5))
        }
        if self.characters.count > 9 {
            self.insert("-", at:self.characters.index(self.startIndex, offsetBy: 9))
        }
        
        if self.characters.count > 0 {
            var lastChar = self.substring(from: self.characters.index(before: self.endIndex))
            print(lastChar)
            while ("()-".contains("\(lastChar)")) {
                self.remove(at: self.characters.index(before: self.endIndex))
                if self.characters.count >= 1 {
                    lastChar = self.substring(from: self.characters.index(before: self.endIndex))
                }
            }
        }
        
     }
    
    mutating func unFormatPhone() {
        self = self.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "  ", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: ") ", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " (", with: "")
        
    }
}
