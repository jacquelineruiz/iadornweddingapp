//
//  InitialCheckList.swift
//  iAdorn
//
//  Created by Jaxs on 5/10/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import SwiftLoader
import FirebaseDatabase

class InitialCheckList: UIViewController, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var checkListTable: UITableView!
    var customListItems = [CheckListProfile]()
    var M12ListItems = [CheckListProfile]()
    var M9ListItems = [CheckListProfile]()
    var M6ListItems = [CheckListProfile]()
    var M3ListItems = [CheckListProfile]()
    var W2ListItems = [CheckListProfile]()
    
    
    var picker: UIPickerView = UIPickerView()
    var pickerItems : NSArray =  ["All", "Custom", "12 - 10 Months", "9 - 7 Months", "6 - 4 Months", "3 - 1 Months", "2 weeks before"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        configurePicker()
        configure()
        filterBtn.layer.cornerRadius = self.filterBtn.frame.size.height/3
        filterBtn.clipsToBounds = true
        
        checkListTable.delegate = self
        checkListTable.separatorColor = UIColor.clear
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkListTable.reloadData()
    }
    
    func configure(){
        SwiftLoader.show(animated: true)
        NetworkManager.sharedInstance.CHECK_LIST_REF.observe(.value, with: {
            snapshot in
            
            self.customListItems.removeAll()
            self.M12ListItems.removeAll()
            self.M9ListItems.removeAll()
            self.M6ListItems.removeAll()
            self.M3ListItems.removeAll()
            self.W2ListItems.removeAll()
            
            //customList
            if (snapshot.childSnapshot(forPath: "customList").exists()) {
                let custom = snapshot.childSnapshot(forPath: "customList")
                for i in (custom.children.allObjects as? [FIRDataSnapshot])! {
                    if let postDictionary = i.value as? Dictionary<String, AnyObject>{
                       print(postDictionary)
                        let listItem = CheckListProfile(list: "customList", key: i.key, dictionary: postDictionary)
                        print(listItem.item)
                        
                        let value = listItem.item
                        
                        //if custom item is added by user,then do not show "Add Item" in tableview
                        if value != "Add Item"{
                        self.customListItems.insert(listItem, at: 0)
                        }
                        
                        //if there are no custom items, then only display "Add Item" if available
                        if custom.childrenCount == 1{
                            if value == "Add Item"{
                                self.customListItems.insert(listItem, at: 0)
                            }
                        }
                        
                        //if no items exist, then insert "Add Item"
                        if custom.childrenCount == 0{
                            let listItem = CheckListProfile(list: "customList", key: "1", dictionary:["false": "Add Item" as AnyObject])
                            self.customListItems.insert(listItem, at: 0)
                        }
                    }else{
                        let listItem = CheckListProfile(list: "customList", key: "1", dictionary:["false": "Add Item" as AnyObject])
                        self.customListItems.insert(listItem, at: 0)
                    }
                }
            }
            
            //12MonthList
            if (snapshot.childSnapshot(forPath: "12MonthList").exists()) {
                let custom = snapshot.childSnapshot(forPath: "12MonthList")
                for item in (custom.children.allObjects as? [FIRDataSnapshot])! {
                    if let postDictionary = item.value as? Dictionary<String, AnyObject>{
                       // print(postDictionary)
                        let listItem = CheckListProfile(list: "12MonthList", key: item.key, dictionary: postDictionary)
                        self.M12ListItems.append(listItem)
                    }
                }
            }
            
            //9MonthList
            if (snapshot.childSnapshot(forPath: "9MonthList").exists()) {
                let custom = snapshot.childSnapshot(forPath: "9MonthList")
                for item in (custom.children.allObjects as? [FIRDataSnapshot])! {
                if let postDictionary = item.value as? Dictionary<String, AnyObject>{
                        //print(postDictionary)
                        let listItem = CheckListProfile(list: "9MonthList", key: item.key, dictionary: postDictionary)
                        self.M9ListItems.append(listItem)
                    }}
            }
            
            //6MonthList
            if (snapshot.childSnapshot(forPath: "6MonthList").exists()) {
                let custom = snapshot.childSnapshot(forPath: "6MonthList")
                for item in (custom.children.allObjects as? [FIRDataSnapshot])! {
                    if let postDictionary = item.value as? Dictionary<String, AnyObject>{
                        //print(postDictionary)
                        let listItem = CheckListProfile(list: "6MonthList", key: item.key, dictionary: postDictionary)
                        self.M6ListItems.append(listItem)
                    }
                }
            }
            
            //3MonthList
            if (snapshot.childSnapshot(forPath: "3MonthList").exists()) {
                let custom = snapshot.childSnapshot(forPath: "3MonthList")
                for item in (custom.children.allObjects as? [FIRDataSnapshot])! {
                    if let postDictionary = item.value as? Dictionary<String, AnyObject>{
                       // print(postDictionary)
                        let listItem = CheckListProfile(list: "3MonthList", key: item.key, dictionary: postDictionary)
                        self.M3ListItems.append(listItem)
                    }
                }
            }
            
            //2WeekList
            if (snapshot.childSnapshot(forPath: "2WeekList").exists()) {
                let custom = snapshot.childSnapshot(forPath: "2WeekList")
                for item in (custom.children.allObjects as? [FIRDataSnapshot])! {
                    
                    if let postDictionary = item.value as? Dictionary<String, AnyObject>{
                       // print(postDictionary)
                        let listItem = CheckListProfile(list: "2WeekList", key: item.key, dictionary: postDictionary)
                        self.W2ListItems.append(listItem)
                    }
                }
            }
            
            self.checkListTable.reloadData()
            SwiftLoader.hide()
            }, withCancel: { (error) -> Void in
                print(error.localizedDescription)
                SwiftLoader.hide()
        })
        self.checkListTable.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: AnyObject) {
        self.slideMenuController()?.openLeft()
        
    }
    
    @IBAction func sortList(_ sender: UIButton) {
        self.view.addSubview(self.picker)

    }
    
    @IBAction func addToCustomList(_ sender: UIButton) {
        
        let customListAlert = UIAlertController(title: "Add Item",
                                      message: "Add new item to Custom List",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) {
            (action: UIAlertAction!) -> Void in
            
            let textField = customListAlert.textFields![0]
            if textField.text != ""{
                let items: Dictionary<NSString, AnyObject> = [
                    "item": textField.text! as NSString,
                    "completed":  false as AnyObject
                    
                ]
                
                SwiftLoader.show(animated: true)
                NetworkManager.sharedInstance.CHECK_LIST_REF.child("customList").observe(.value, with: {
                    snapshot in
                    //print(snapshot.value)
                    
                    
                    //FIX
                    // when new item added, erase "Add Item"
                })
                
                //Add new item
                NetworkManager.sharedInstance.CHECK_LIST_REF.child("customList").childByAutoId().setValue(items)
                SwiftLoader.hide()
            }
        }
        let cancelAction =  UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        customListAlert.addTextField {
            (textField: UITextField!) -> Void in
        }
        
        customListAlert.addAction(saveAction)
        customListAlert.addAction(cancelAction)
        present(customListAlert, animated: true, completion: nil)
    }
    
    func configurePicker(){
        self.picker.dataSource = self
        self.picker.frame = CGRect(x: 0, y: 25, width: self.view.frame.width, height: 100)
        self.picker.backgroundColor = iAdornConstants.fushiaColor
        self.picker.tintColor = UIColor.white
    }
    
    //MARK: UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerItems.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerItems[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //print(pickerItems[row] as? String)
        
        let result = pickerItems[row] as? String
                    filterBtn.setTitle(result, for: UIControlState())
        
        picker.removeFromSuperview()
        viewDidLoad()
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: pickerItems[row] as! String, attributes: [NSForegroundColorAttributeName : UIColor.white])
        return attributedString
    }

    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return iAdornConstants.checkList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return customListItems.count
        }else if section == 1{
            return M12ListItems.count
        }else if section == 2{
            return M9ListItems.count
        }else if section == 3{
            return M6ListItems.count
        }else if section == 4{
            return M3ListItems.count
        }else{
            return W2ListItems.count
        }
     }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < iAdornConstants.checkList_headerTitles.count {
            return iAdornConstants.checkList_headerTitles[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = iAdornConstants.fushiaColor
        let headerView: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        headerView.textLabel!.textColor = UIColor.white
    }
 
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckList") as! CheckListTableViewCell

        if (indexPath as NSIndexPath).section == 0{
            let listItem = customListItems[(indexPath as NSIndexPath).row]
            cell.configureCell(listItem, section: "customList")
            
        }else if (indexPath as NSIndexPath).section == 1{
            let listItem = M12ListItems[(indexPath as NSIndexPath).row]
            cell.configureCell(listItem, section: "12MonthList")
            
        }else if (indexPath as NSIndexPath).section == 2{
            let listItem = M9ListItems[(indexPath as NSIndexPath).row]
            cell.configureCell(listItem, section: "9MonthList")
            
        }else if (indexPath as NSIndexPath).section == 3{
            let listItem = M6ListItems[(indexPath as NSIndexPath).row]
            cell.configureCell(listItem, section: "6MonthList")
            
        }else if (indexPath as NSIndexPath).section == 4{
            let listItem = M3ListItems[(indexPath as NSIndexPath).row]
            cell.configureCell(listItem, section: "3MonthList")
            
        }else if (indexPath as NSIndexPath).section == 5{
            let listItem = W2ListItems[(indexPath as NSIndexPath).row]
            cell.configureCell(listItem, section: "2WeekList")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool{
        return (indexPath as NSIndexPath).section == 0 ? true : false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        var listItem : CheckListProfile!
        if (indexPath as NSIndexPath).section == 0{
            listItem = customListItems[(indexPath as NSIndexPath).row]
            
        }else if (indexPath as NSIndexPath).section == 1{
            listItem = M12ListItems[(indexPath as NSIndexPath).row]
            
        }else if (indexPath as NSIndexPath).section == 2{
            listItem = M9ListItems[(indexPath as NSIndexPath).row]
            
        }else if (indexPath as NSIndexPath).section == 3{
            listItem = M6ListItems[(indexPath as NSIndexPath).row]
            
        }else if (indexPath as NSIndexPath).section == 4{
            listItem = M3ListItems[(indexPath as NSIndexPath).row]
            
        }else if (indexPath as NSIndexPath).section == 5{
            listItem = W2ListItems[(indexPath as NSIndexPath).row]
        }
        
        //DELETE
        let delete = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            let alert_DeleteCustomItem = UIAlertController(title: "Warning", message: "Are you sure you want to delete this item?", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) -> Void in
                
                SwiftLoader.show(animated: true)
                //observeSingleEventOfType reads data once. This is different then other listeners.
                NetworkManager.sharedInstance.CHECK_LIST_REF.child("customList").observeSingleEvent(of: .value, with: { snapshot in
                    SwiftLoader.show(animated: true)
                    for item in (snapshot.children.allObjects as? [FIRDataSnapshot])! {
                        let currentItem = (item.value! as AnyObject).object(forKey: "item") as! String
                        if currentItem  == listItem.item{
                            NetworkManager.sharedInstance.CHECK_LIST_REF.child("customList/\(item.key)").removeValue()
                        }
                    }
                    SwiftLoader.hide()
                    }, withCancel: { (error) -> Void in
                        print(error.localizedDescription)
                        SwiftLoader.hide()
                })
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alert_DeleteCustomItem.addAction(okAction)
            alert_DeleteCustomItem.addAction(cancelAction)
            self.present(alert_DeleteCustomItem, animated: true, completion: nil)
        }
        
        return [delete]
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    

}
