//
//  HomeViewController.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/26/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import SwiftLoader
import FirebaseDatabase

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //WORK IN PROGRESS - NOT CURRENTLY IMPLEMENTED
    
    @IBOutlet weak var welcome: UILabel!
    @IBOutlet weak var weddingCountDownLabel: UILabel!
    @IBOutlet weak var homeTableView: UITableView!
  
    var feedItem = [HomeFeedProfile]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.weddingCountDownLabel.isHidden = true
        self.welcome.isHidden = true
        homeTableView.separatorColor = UIColor.clear
        configureWelcomeMessage()
        configureTableView()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func configureWelcomeMessage(){
        SwiftLoader.show(animated: true)
        NetworkManager.sharedInstance.CURRENT_USER_REF.observe(.value, with: { snapshot in
            //print(snapshot.value)
            let name = (snapshot.value! as AnyObject).object(forKey: "username") as! String
            let stringName = name
            if name != ""{
                self.welcome.text = "Welcome \(stringName)"
                self.welcome.isHidden = false
            }
            
//            let weddate  = snapshot.value!["weddingDate"]
//            let date = weddate as? String
//            
//            if date != nil{
//            self.calculateDaysRemaining(date!)
//            }
                      SwiftLoader.hide()
            }, withCancel: { (error) -> Void in
                print(error.localizedDescription)
                SwiftLoader.hide()
        })
    }
    
    func calculateDaysRemaining(_ date :String){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        let today = Date()
        let weddingDate = dateFormatter.date(from: date)
        let daysRemaining = weddingDate!.daysFrom(today)
        if daysRemaining == 0{
           self.weddingCountDownLabel.text  = "1 day left til your wedding!"
        }else if daysRemaining == -1{
            self.weddingCountDownLabel.text  = "Today is the big day!"
        }else if daysRemaining < -1{
            self.weddingCountDownLabel.text  = "Congratulations on your wedding!"
        }else{
            self.weddingCountDownLabel.text  = "\(daysRemaining) days til your wedding!"
        }
        self.weddingCountDownLabel.isHidden = false
    }
    
    func configureTableView(){
        SwiftLoader.show(animated: true)
        NetworkManager.sharedInstance.CURRENT_USER_HOME_FEED.observe(.value, with: {
            snapshot in
            
            self.feedItem = []
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot]{
                for snap in snapshots{
                    if let postDictionary = snap.value as? Dictionary<String, AnyObject>{
                        //print(postDictionary)
                        
                        let key = snap.key
                        let guest = HomeFeedProfile(key: key , dictionary: postDictionary)
                        // Items are returned chronologically, but here the newest guest is first.
                        
                        self.feedItem.insert(guest , at: 0)
                    }
                }
            }
            // Be sure that the tableView updates when there is new data.
            self.homeTableView.reloadData()
            SwiftLoader.hide()
        })
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sideMenu(_ sender: UIButton) {
       slideMenuController()?.openLeft()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFeedCell") as! HomeFeedTableViewCell    
        let feed = feedItem[(indexPath as NSIndexPath).row]
        
        cell.configureCell(feed)
        return cell
    }
}
