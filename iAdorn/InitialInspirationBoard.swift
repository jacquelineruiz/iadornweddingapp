//
//  InitialInspirationBoard.swift
//  iAdorn
//
//  Created by Jaxs on 5/10/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import PinterestSDK
import AVFoundation
import CHTCollectionViewWaterfallLayout
import SwiftLoader

class InitialInspirationBoard: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, CHTCollectionViewDelegateWaterfallLayout {
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var pdkCollection: UICollectionView!
    var pins = [AnyObject]()
    var currentResponseObject = PDKResponseObject()
    var board = PDKBoard()
    var fetchingMore = Bool()
    var pdkObjectCount = 100
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.delegate = self
        
        authenticate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.pdkCollection.isPagingEnabled = true
        pins = [AnyObject]()
    }
    
    func authenticate(){
        let permissions = [ PDKClientReadPublicPermissions,
                            PDKClientWritePublicPermissions,
                            PDKClientReadPrivatePermissions,
                            PDKClientWritePrivatePermissions,
                            PDKClientReadRelationshipsPermissions,
                            PDKClientWriteRelationshipsPermissions]
        
        SwiftLoader.show(animated: true)
        PDKClient.sharedInstance().authenticate(withPermissions: permissions, withSuccess: { (responseObject) -> Void in
            print("success PDKResponseObject: \(responseObject)")
            self.updateSearch()
            
            
            //check if user has "iAdorn" board
            //if user does not have board create one.
            
             PDKClient.sharedInstance().createBoard("iAdorn", boardDescription: "Wedding Planner", withSuccess: { (responseObject) -> Void in
                print("success PDKResponseObject: \(responseObject)")
                

                }, andFailure: { (err  ) -> Void in
                    print("error NSError: \(err?.localizedDescription)")
                    
                    SwiftLoader.hide()
            })
            
        }) { (err) -> Void in
            print("error NSError: \(err?.localizedDescription)")
//            if err.code == 429{
//                showAlertWithOKButton("Error", message: "Too many requests./nPlease try again later.", presentingViewController: self)
//            }
            SwiftLoader.hide()
        }
    }
    
    func updateSearch(){
        
        var parameter = [String : String]()
        var path = String()
        if self.searchField.text == ""{
            parameter = ["query": "wedding", "fields": "url, link, image, note", "limit" : "100"]
        }else{
            parameter = ["query": "\(self.searchField.text!)", "fields": "image, note", "limit" : "100"]
            
        }
       
        print(parameter)
        PDKClient.sharedInstance().getPath("me/search/pins" , parameters: parameter, withSuccess: { (pdk) -> Void in
            print("success \(pdk)")
            
            // loads the first set of pins
            self.currentResponseObject = pdk!
            self.pins = pdk!.pins() as [AnyObject]
            
            // loads additional pins
            if self.currentResponseObject.hasNext(){
                self.currentResponseObject.loadNext(success: { (pdkHasNext) -> Void in
                    for i in (pdkHasNext?.pins())!{
                        self.pins.append(i as AnyObject)
                        
                    }
                    print("added pins")
                    print("\(self.pins.count) pins total")
                    
                    }, andFailure: {
                        (err ) -> Void in
                        print("error NSError: \(err?.localizedDescription)")
                        
                })
            }
            
            
            
            
            //Layout setup
            self.setupCollectionView()
            
            //Register nibs
            self.registerNibs()
            SwiftLoader.hide()
            }, andFailure: { (err) -> Void in
                print("error NSError: \(err?.localizedDescription)")
                showAlertWithOKButton("Error", message: "Please try again.", presentingViewController: self)
                SwiftLoader.hide()
        })
       
    }
    
    @IBAction func menuAction(_ sender: AnyObject) {
        self.slideMenuController()?.openLeft()
    }
    
    //MARK: - CollectionView UI Setup
    func setupCollectionView(){
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        layout.minimumInteritemSpacing = 10.0
        
        // Collection view attributes
        self.pdkCollection.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.pdkCollection.alwaysBounceVertical = true
        
        DispatchQueue.main.async(execute: {
            
            self.pdkCollection?.reloadData()
        })
        
        // Add the waterfall layout to your collection view
        self.pdkCollection.collectionViewLayout = layout
    }
    
    // Register CollectionView Nibs
    func registerNibs(){
        
        // attach the UI nib file for the ImageUICollectionViewCell to the collectionview
        let viewNib = UINib(nibName: "AnnotatedPhotoCell", bundle: nil)
        pdkCollection.register(viewNib, forCellWithReuseIdentifier: "cell")
    }
    
    
    //MARK: - CollectionView Waterfall Layout Delegate Methods (Required)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // create a cell size from the image size, and return the size
        let pin = self.pins[(indexPath as NSIndexPath).row]
        return CGSize(width: pin.largestImage().width, height: pin.largestImage().height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
       //PDK initial PDKResponseObject is limited to 100
        
        if (indexPath as NSIndexPath).row == pdkObjectCount - 5 {
            // handle your logic here to get more items, add it to dataSource and reload tableview
             getRemainingPinsAndImages()
        }
    }
    
    func getRemainingPinsAndImages() {
        
        if self.fetchingMore == false && self.currentResponseObject.hasNext()  {
            
            
            self.fetchingMore = true
            
            self.currentResponseObject.loadNext(success: { (nextResponseObject) -> Void in
                
                
                self.fetchingMore = false
                self.currentResponseObject = nextResponseObject!
               
                guard let pinsAsPDKPin = nextResponseObject?.pins() as? [PDKPin] else { return }
                
                for pin in pinsAsPDKPin {
                    self.pins.append(pin)
                }
                
                print("\(self.pins.count) new total pins")
                self.pdkObjectCount = self.pins.count
                
                    DispatchQueue.main.async(execute: {
                        
                        self.pdkCollection?.reloadData()
                    })
                
            }) { (err ) -> Void in
                print("error NSError: \(err?.localizedDescription)")
            }
            
        }
    }

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return pins.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
//        let pin = self.pins[indexPath.row] as! PDKPin
        
        let pin = self.pins[(indexPath as NSIndexPath).row]
//        UIApplication.sharedApplication().openURL(pin.url)
//        pdkCollection.deselectItemAtIndexPath(indexPath, animated: true)
        
        
        
        
//        app crashes b/c of one of these parameters
//        PDKPin.pinWithImageURL(pin.largestImage().url, link: pin.pinURL , suggestedBoardName: "iAdorn", note: pin.descriptionText, withSuccess: {
//            print("success")
//            
//            }, andFailure: { (err :NSError!) -> Void in
//                print("error NSError: \(err)")
//                showAlertWithOKButton("Error", message: "Please try again.", presentingViewController: self)
//                SwiftLoader.hide()
//        })
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnotatedPhotoCell", for: indexPath) as! AnnotatedPhotoCell
        
        let pin = self.pins[(indexPath as NSIndexPath).row]
        //print(self.pins.count)
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        cell.pdkImage.setImageWith(pin.largestImage().url)
        cell.captionLabel.text = pin.descriptionText
        cell.captionLabel.sizeToFit()
        
        //Fade in image and caption
        cell.pdkImage.alpha = 0
        cell.captionLabel.alpha = 0
        UIView.animate(withDuration: 3, animations: {
            cell.pdkImage.alpha = 1
            cell.captionLabel.alpha = 1
        })

        return cell
    }
    
    //MARK - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.updateSearch()
        
        return true
    }
}
