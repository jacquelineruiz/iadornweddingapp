//
//  UIImage.swift
//  iAdorn
//
//  Created by Jaxs on 6/13/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import Foundation
import UIKit


extension UIImage{
    var decompressedImage: UIImage {
        UIGraphicsBeginImageContextWithOptions(size, true, 0)
        draw(at: CGPoint.zero)
        let decompressedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return decompressedImage!
    }
}
