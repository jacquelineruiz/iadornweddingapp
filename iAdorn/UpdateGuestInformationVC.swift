//
//  UpdateGuestInformationVC.swift
//  iAdorn
//
//  Created by Jaxs on 5/23/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import SwiftLoader
import FirebaseDatabase

class UpdateGuestInformationVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate  {

    var key: String!
    var name: String!
    var street: String!
    var city: String!
    var state: String!
    var zipcode: String!
    var guestCnt: Int!
    var phone: String!
    var emailString: String!
    var partyStatus: String!
    var rsvpStatus: String!
    
    @IBOutlet weak var assignGuestToParty: UIButton!
    @IBOutlet weak var assignRSVPStatus: UIButton!
    @IBOutlet weak var guestName: UITextField!
    @IBOutlet weak var guestStreet: UITextField!
    @IBOutlet weak var guestCity: UITextField!
    @IBOutlet weak var guestState: UITextField!
    @IBOutlet weak var guestZipCode: UITextField!
    @IBOutlet weak var guestEmailAddress: UITextField!
    @IBOutlet weak var guestPhoneNum: UITextField!
    @IBOutlet weak var guestCount: UITextField!
    
    var guestPartyPicker: UIPickerView = UIPickerView()
    var guestParty : NSArray =  ["Groom's Guest", "Bride's Guest"]
    var rsvpPicker: UIPickerView = UIPickerView()
    var rsvp : NSArray =  ["Regret", "Accept"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guestName.delegate = self
        guestStreet.delegate = self
        guestCity.delegate = self
        guestState.delegate = self
        guestZipCode.delegate = self
        guestPhoneNum.delegate = self
        guestEmailAddress.delegate = self
        guestCount.delegate = self
        

        assignGuestToParty.layer.cornerRadius = self.assignGuestToParty.frame.size.height/3
        assignGuestToParty.clipsToBounds = true
        
        assignRSVPStatus.layer.cornerRadius = self.assignRSVPStatus.frame.size.height/3
        assignRSVPStatus.clipsToBounds = true

        
        self.guestPartyPicker.delegate = self
        self.rsvpPicker.delegate = self
        configurePicker()
        
        if name != nil {
            self.guestName.text = name
        }
        
        if street != nil {
            self.guestStreet.text = street
        }
        
        if city != nil {
            self.guestCity.text = city
        }
        
        if state != nil {
            self.guestState.text = state
        }
        
        if zipcode != nil {
            self.guestZipCode.text = zipcode
        }
        
        if emailString != nil {
            self.guestEmailAddress.text = emailString
        }
        
        if phone != nil {
            self.guestPhoneNum.text = phone
        }
        
        if guestCnt != 0 {
            self.guestCount.text = String(guestCnt)
        }else{
            self.guestCount.placeholder = "# of Guest"
        }
        
        if partyStatus != nil {
            assignGuestToParty.setTitle(partyStatus, for: UIControlState())
         }
        
        if rsvpStatus != nil {
            assignRSVPStatus.setTitle(rsvpStatus, for: UIControlState())
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configurePicker(){
        self.guestPartyPicker.dataSource = self
        self.guestPartyPicker.frame = CGRect(x: 0, y: 340, width: self.view.frame.width, height: 100)
        self.guestPartyPicker.backgroundColor = iAdornConstants.fushiaColor
        self.guestPartyPicker.tintColor = UIColor.white
        
        self.rsvpPicker.dataSource = self
        self.rsvpPicker.frame = CGRect(x: 0, y: 340, width: self.view.frame.width, height: 100)
        self.rsvpPicker.backgroundColor = iAdornConstants.fushiaColor
        self.rsvpPicker.tintColor = UIColor.white
    }
    
    @IBAction func addFromUsersContacts(_ sender: UIButton) {
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func assignGuestToParty(_ sender: UIButton) {
        self.view.addSubview(self.guestPartyPicker)
    }
    
    @IBAction func assignRSVPStatus(_ sender: UIButton) {
        self.view.addSubview(self.rsvpPicker)
    }
    
    @IBAction func updateGuest(_ sender: UIButton) {
        SwiftLoader.show(animated: true)
        var guestCnt = 0
        if self.guestCount.text! != ""{
            guestCnt = Int(self.guestCount.text!)!
        }
        
        var rsvp = ""
        if self.assignRSVPStatus.titleLabel!.text! == "RSVP Pending"{
            rsvp = "Regret"
        }else{
           rsvp = self.assignRSVPStatus.titleLabel!.text!
        }
        
        let updatedGuestInfo:  Dictionary<String, AnyObject> = [
            "guestname": self.guestName.text! as AnyObject,
            "guestAddress_Street": self.guestStreet.text! as AnyObject,
            "guestAddress_City": self.guestCity.text! as AnyObject,
            "guestAddress_State": self.guestState.text! as AnyObject,
            "guestAddress_ZipCode": self.guestZipCode.text! as AnyObject,
            "guestEmailAddress": self.guestEmailAddress.text! as AnyObject,
            "guestPhoneNumber": self.guestPhoneNum.text! as AnyObject,
            "guestFamilySize": guestCnt as AnyObject,
            "guestRSVPStatus": rsvp as AnyObject,
            "assignGuestToParty": (self.assignGuestToParty.titleLabel?.text)! as AnyObject
        ]
        print(key!)
        
        
        
        
        NetworkManager.sharedInstance.Current_User_Guests.child(key!).updateChildValues(updatedGuestInfo, withCompletionBlock: {
            (error , ref) in
            
            if (error != nil) {
                showAlertWithOKButton("Alert!", message: "Data could not be saved at this time.", presentingViewController: self)
            } else {
                self.dismiss(animated: true, completion: nil)
                
                //update Home Feed
                let date = Date()
                // create a new feed for the home scene.
                let newHomeFeed: Dictionary<String, AnyObject> = [
                    "_feedMessage": "\(self.guestName.text!)'s profile was updated" as AnyObject,
                    "_feedDate": date.formatAsStringWithTime as AnyObject
                ]
                // Send it over to DataService to seal the deal.
                //print(newHomeFeed)
                NetworkManager.sharedInstance.createHomeFeed(newHomeFeed)
            }
            SwiftLoader.hide()
        })

        
    }
    
    //MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == guestPartyPicker{
            return guestParty.count
        }else {
            return rsvp.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == guestPartyPicker{
            return guestParty[row] as? String
        }else {
            return rsvp[row] as? String
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == guestPartyPicker{
            //print(guestParty[row] as? String)
            let result = guestParty[row] as? String
            assignGuestToParty.setTitle(result, for: UIControlState())
            guestPartyPicker.removeFromSuperview()
        }else {
            //print(rsvp[row] as? String)
            let result = rsvp[row] as? String
            assignRSVPStatus.setTitle(result, for: UIControlState())
            rsvpPicker.removeFromSuperview()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if pickerView == guestPartyPicker{
            let attributedString = NSAttributedString(string: guestParty[row] as! String, attributes: [NSForegroundColorAttributeName : UIColor.white])
            return attributedString
        }else {
            let attributedString = NSAttributedString(string: rsvp[row] as! String, attributes: [NSForegroundColorAttributeName : UIColor.white])
            return attributedString
        }
    }
}
