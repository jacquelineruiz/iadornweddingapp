//
//  Profile.swift
//  iAdorn
//
//  Created by Jaxs on 5/10/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import MobileCoreServices
import Firebase
import FirebaseDatabase
import FirebaseAuth
import SwiftLoader

class Profile: UIViewController, UIImagePickerControllerDelegate,  UINavigationControllerDelegate, UIPickerViewDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var bottomSpaceContraint: NSLayoutConstraint!
    @IBOutlet weak var usersPhoto: UIButton!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var spouseName: UITextField!
    @IBOutlet weak var phoneNum: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var weddingDateLabel: UILabel!
    @IBOutlet weak var setWeddingDateBtn: UIButton!
    
    let imagePicker = UIImagePickerController()
    var newMedia: Bool?
    
    let datePicker = UIDatePicker()
    var doneButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        updateDatePicker()
        
        self.userName.delegate = self
        self.spouseName.delegate = self
        self.phoneNum.delegate = self
        self.email.delegate = self
        
        imagePicker.delegate = self
        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.allowsEditing = true
        
        usersPhoto.layer.cornerRadius = self.usersPhoto.frame.size.height/3
        usersPhoto.clipsToBounds = true
        
        setWeddingDateBtn.layer.cornerRadius = self.setWeddingDateBtn.frame.size.height/3
        setWeddingDateBtn.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification:Notification) {
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        bottomSpaceContraint.constant = keyboardHeight + 5

        self.updateView(false)
        
    }
    
    func keyboardWillHide(_ notification:Notification) {
        bottomSpaceContraint.constant = 93.0
        self.updateView(true)
    }
    
    fileprivate func updateView(_ keyboardHidden: Bool) {
        
        UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            
            self.usersPhoto.alpha = self.bottomSpaceContraint.constant == 93.0 ? 1:0
            self.view.layoutIfNeeded()
            
            }, completion: { (finished: Bool) -> Void in
                
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneBtn(_ sender: UIButton) {
        
        let storyboard:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.slideMenuController()?.changeMainViewController(vc, close: true)
    }
    
    func configure(){
        SwiftLoader.show(animated: true)
        NetworkManager.sharedInstance.CURRENT_USER_REF.observe(.value, with: { snapshot in
            //print(snapshot.value)
            
            let weddingDate  = (snapshot.value! as AnyObject).object(forKey: "weddingDate") as! String
            //snapshot.value!["weddingDate"]
            self.weddingDateLabel.text = weddingDate
            
            let name = (snapshot.value! as AnyObject).object(forKey: "username") as! String
            //snapshot.value!["username"]
            self.userName.text = name
            
            let spouseName = (snapshot.value! as AnyObject).object(forKey: "spousename") as! String
            //snapshot.value!["spousename"]
            self.spouseName.text = spouseName
            
            let phoneNumber = (snapshot.value! as AnyObject).object(forKey: "phonenumber") as! String
            //snapshot.value!["phonenumber"]
            self.phoneNum.text = phoneNumber
            
            let email = (snapshot.value! as AnyObject).object(forKey: "email") as! String
            //snapshot.value!["email"]
            self.email.text = email
            
            let userImage = (snapshot.value! as AnyObject).object(forKey: "userImage") as! String
            //snapshot.value!["userImage"]
            // let image = userImage
            
            if userImage.isEmpty {
                return
            }else{
                let image = userImage 
                let decodedData = Data(base64Encoded: image, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                self.usersPhoto.setImage(UIImage(data: decodedData!), for: UIControlState())
            }
            }, withCancel: { (error) -> Void in
                print(error.localizedDescription)
        })
        SwiftLoader.hide()
    }
    
    @IBAction func saveProfileChanges(_ sender: UIButton) {
        SwiftLoader.show(animated: true)
        
        var weddingDate = String()
        var spousename = String()
        var phone = String()
        
        if ((self.weddingDateLabel.text?.isEmpty) != nil){
            weddingDate = self.weddingDateLabel.text!
        }else{
            weddingDate = ""
        }
        
        if self.spouseName.text!.isEmpty{
            spousename = ""
        }else{
            spousename = self.spouseName.text!
        }
        
        if self.phoneNum.text!.isEmpty{
            phone = ""
        }else{
            phone = self.phoneNum.text!
        }
        
       
        let updatedUserInfo: Dictionary<String, AnyObject> = [
            "weddingDate": weddingDate as AnyObject,
            "username": self.userName.text! as AnyObject,
            "spousename": spousename as AnyObject,
            "phonenumber": phone as AnyObject,
            "email": self.email.text! as AnyObject
        ]
        
        
        let date = Date()
        // create a new feed for the home scene.
        let newHomeFeed: Dictionary<String, AnyObject> = [
            "_feedMessage": "Your Profile was updated" as AnyObject,
            "_feedDate": date.formatAsStringWithTime as AnyObject
        ]
        // Send it over to DataService to seal the deal.
        //print(newHomeFeed)
        NetworkManager.sharedInstance.createHomeFeed(newHomeFeed)
        
        
        NetworkManager.sharedInstance.CURRENT_USER_REF.updateChildValues(updatedUserInfo, withCompletionBlock:{
            (error, ref ) in
            
            if ((error) != nil){
                let message = "\(error?.localizedDescription)"
                showAlertWithOKButton("Error", message: message, presentingViewController: self)
            }else{
                 
                let storyboard:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        })
        SwiftLoader.hide()
    }
  
 
    @IBAction func setWeddingDate(_ sender: UIButton) {
        self.view.addSubview(doneButton) // add Button to UIView
        self.view.addSubview(datePicker) // add datePicker to UIView
    }
    
    func updateDatePicker(){
        doneButton = UIButton(frame: CGRect(x: 0, y: 25, width: self.view.frame.width, height: 35))
        doneButton.backgroundColor = UIColor.white
        doneButton.setTitle("Done", for: UIControlState())
        doneButton.setTitleColor(UIColor.black, for: UIControlState())
        doneButton.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        // set button click event
        doneButton.addTarget(self, action: #selector(Profile.closeDatePicker(_:)), for: UIControlEvents.touchUpInside)
        
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.frame = CGRect(x: 0, y: 60, width: self.view.frame.width, height: 150)
        datePicker.backgroundColor = UIColor.white
        datePicker.layer.cornerRadius = 5.0
        datePicker.layer.shadowOpacity = 0.5
        
        // add an event called when value is changed.
        datePicker.addTarget(self, action: #selector(Profile.onDidChangeDate(_:)), for: .valueChanged)
    }
    
    func closeDatePicker(_ sender:UIButton){
        doneButton.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
    
    func onDidChangeDate(_ sender: UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.timeStyle = DateFormatter.Style.none

        let selectedDate: NSString = dateFormatter.string(from: sender.date) as NSString
        self.weddingDateLabel.text = selectedDate as String
    }
    
    @IBAction func resetPassword(_ sender: UIButton) {
        
        let passwordResetAlert = UIAlertController(title: "Reset Password",
                                                message: nil,
                                                preferredStyle: .alert)
        
        passwordResetAlert.addTextField {
            (userEmail: UITextField!) -> Void in
            userEmail.placeholder = "Email Address"
            if self.email.text != ""{
                userEmail.text = self.email.text
            }else{
                userEmail.text = ""
            }
        }
        
        passwordResetAlert.addTextField {
            (newPassword: UITextField!) -> Void in
            newPassword.placeholder = "New Password"
         }
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            //let uEmail = passwordResetAlert.textFields![0]
            let newPassword = passwordResetAlert.textFields![1]
             if newPassword.text != ""{
                
                let user = FIRAuth.auth()?.currentUser
                user?.updatePassword(newPassword.text!) { error in
                    if let error = error {
                        // there was an error
                         showAlertWithOKButton("Alert", message: "\(error.localizedDescription)", presentingViewController: self)
                    } else {
                        // Password updated
                        showAlertWithOKButton("", message: "Password changed successfully", presentingViewController: self)
                    }
                }
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        passwordResetAlert.addAction(okAction)
        passwordResetAlert.addAction(cancelAction)
        self.present(passwordResetAlert, animated: true, completion: nil)
    }
    
    
    @IBAction func changeUserPhoto(_ sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let action1 = UIAlertAction(title: "Camera", style: .default, handler: {(action) in
            self.goToCamera()
        })
        let action2 = UIAlertAction(title: "Photo Album", style: .default, handler: {(action) in
            self.goToPhotoAlbum()
        })
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.view.tintColor = iAdornConstants.fushiaColor
        present(alert, animated: true, completion: nil)
        
        // see link re: tintColor appearing twice
        // stackoverflow.com/a/32636878/4376309
        alert.view.tintColor = iAdornConstants.fushiaColor
    }
    
    func goToCamera(){
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.camera) {
            
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            
            imagePicker.showsCameraControls = true
            
            self.present(imagePicker, animated: true,
                                       completion: nil)
            newMedia = true
        }
    }
    
    func goToPhotoAlbum(){
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.savedPhotosAlbum) {
            
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            
            self.present(imagePicker, animated: true,
                                       completion: nil)
            newMedia = false
        }
    }
    
    func image(_ image: UIImage, didFinishSavingImageToPhotoAlbum error: NSError?, contextInfo:UnsafeRawPointer) {
        if error != nil {
            showAlertWithOKButton("Error Saving Image", message: (error?.localizedDescription)!, presentingViewController: self)
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        if mediaType == kUTTypeImage as String {
            let image = info[UIImagePickerControllerEditedImage] as! UIImage
            usersPhoto.setBackgroundImage(image as UIImage?, for: UIControlState())
            usersPhoto.contentMode = .scaleAspectFit
            
            //save image to Firebase
            var data: Data = Data()
            data = UIImageJPEGRepresentation(image,0.1)!
            let base64String = data.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            NetworkManager.sharedInstance.CURRENT_USER_REF.child("userImage").setValue(base64String)
            
            //add image to users device photo album
            if (newMedia == true) {
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(Profile.image(_:didFinishSavingImageToPhotoAlbum:contextInfo:)), nil)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
