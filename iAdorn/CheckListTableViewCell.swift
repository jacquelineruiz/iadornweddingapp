//
//  CheckListTableViewCell.swift
//  iAdorn
//
//  Created by Jaxs on 5/20/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import SwiftLoader

class CheckListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var item: UILabel!
    
    var listItem: CheckListProfile!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(CheckListTableViewCell.cellTapped(_:)))
        tap.numberOfTapsRequired = 1
        item.addGestureRecognizer(tap)
        item.isUserInteractionEnabled = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ listItem: CheckListProfile, section: String) {
        self.listItem = listItem
        self.item.text = "\(listItem.item)"
        let key = self.listItem.itemKey
        
        NetworkManager.sharedInstance.CHECK_LIST_REF.child("\(section)/\(key)").observeSingleEvent(of: .value
            , with: { snapshot in
                let value = (snapshot.value! as AnyObject).object(forKey: "item") as! NSString
                let status = (snapshot.value! as AnyObject).object(forKey: "completed") as! Bool
                var stat =  NSString()
                if status   {
                    stat = "true"
                }else{
                    stat = "false"
                }
                if stat == "false" ||  value == "Add Item"{
                    self.accessoryType = UITableViewCellAccessoryType.none
                    self.item.textColor = UIColor.black
                } else {
                    self.tintColor = iAdornConstants.fushiaColor
                    self.accessoryType = UITableViewCellAccessoryType.checkmark
                    self.item.textColor = UIColor.gray   
                }
            }, withCancel: { (error) -> Void in
                print(error.localizedDescription)
        })
    }
    
    func cellTapped(_ sender: UITapGestureRecognizer) {
        let key = self.listItem.itemKey
        let section = self.listItem.section
        let task = self.listItem.item
        let completed = self.listItem.completed
        let item = self.listItem.item
        if item == "Add Item"{
            return
        }
        if completed{
            let alert_rsvp = UIAlertController(title: "RSVP Status", message: "Are you sure you would like to uncheck this item?", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction) -> Void in
                SwiftLoader.show(animated: true)
                NetworkManager.sharedInstance.CHECK_LIST_REF.child("\(section)/\(key)").observeSingleEvent(of: .value
                    , with: { snapshot in
                        print("snapshot in cellTapped()")
                        print(snapshot )
                        
                        self.accessoryType = UITableViewCellAccessoryType.none
                        self.item.textColor = UIColor.black
                        self.listItem.updatCompletedStatus(false)
                        
                        let date = Date()
                        // create a new feed for the home scene.
                        let newHomeFeed: Dictionary<String, AnyObject> = [
                            "_feedMessage": "\'\(task)\' was updated as \"Not Completed\"" as AnyObject,
                            "_feedDate": date.formatAsStringWithTime as AnyObject
                        ]
                        // Send it over to DataService to seal the deal.
                        print(newHomeFeed)
                        NetworkManager.sharedInstance.createHomeFeed(newHomeFeed)

                })
                SwiftLoader.hide()
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alert_rsvp.addAction(okAction)
            alert_rsvp.addAction(cancelAction)
            self.parentViewController?.present(alert_rsvp, animated: true, completion: nil)
        }else{
            self.tintColor = iAdornConstants.fushiaColor
            self.accessoryType = UITableViewCellAccessoryType.checkmark
            self.item.textColor = UIColor.gray
            self.listItem!.updatCompletedStatus(true)
            
            
            let date = Date()
            // create a new feed for the home scene.
            let newHomeFeed: Dictionary<String, AnyObject> = [
                "_feedMessage": "\'\(task)\' was marked as \"Completed!\"" as AnyObject,
                "_feedDate": date.formatAsStringWithTime as AnyObject
            ]
            // Send it over to DataService to seal the deal.
            //print(newHomeFeed)
            NetworkManager.sharedInstance.createHomeFeed(newHomeFeed)
            
        }
    }
}
