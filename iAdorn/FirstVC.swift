//
//  FirstVC.swift
//  iAdorn
//
//  Created by Jaxs on 6/25/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import SwiftLoader
import FirebaseAuth

class FirstVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkIfUserIsAlreadySignedIn()
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkIfUserIsAlreadySignedIn() {
        
        if let user = FIRAuth.auth()?.currentUser {
            print(user.displayName)
            
            if let uid = UserDefaults.standard.value(forKey: "uid")  {
                print(uid)
                // User is signed in.
                let storyboard:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "ContainerSlideMenu") as? ContainerSlideMenu
                    else{return}
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(vc, animated: true, completion: nil)
            }else{
                goToInitialVC()
            }
        } else{
            goToInitialVC()
        }
    }
    
    func goToInitialVC() {
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        guard let vc = storyboard.instantiateViewController(withIdentifier: "InitialVC") as? InitialVC
            else{return}
        print("Going to InitialVC")
        self.navigationController?.pushViewController(vc, animated: true)        
    }
}
