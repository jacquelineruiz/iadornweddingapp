//
//  InitialGuestListVC.swift
//  iAdorn
//
//  Created by Jaxs on 5/13/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import SwiftLoader
import FirebaseAuth
import FirebaseDatabase

class InitialGuestListVC: UIViewController, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var guestTableView: UITableView!
    @IBOutlet weak var totalGuestCnt: UILabel!
    @IBOutlet weak var filterBtn: UIButton!
    var guestPicker: UIPickerView = UIPickerView()
    var pickerItems : NSArray =  ["All Guests", "Groom's Guest", "Bride's Guest", "Accept Invitation", "Regret Invitation", "Export 'All Guests'"]

    var guests = [GuestProfile]()
    var guestTotal = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.guestPicker.delegate = self
        configurePicker()

        guestTableView.delegate = self
        guestTableView.allowsSelection = false 
        filterBtn.layer.cornerRadius = self.filterBtn.frame.size.height/3
        filterBtn.clipsToBounds = true
        filterBtn.titleLabel?.text = "All Guests"
        guestTableView.separatorColor = UIColor.clear
        configureGuests()
    }
    
    func configureGuests(){
        SwiftLoader.show(animated: true)
        NetworkManager.sharedInstance.Current_User_Guests.observe(.value, with: {
            snapshot in
            
            self.guests = []
            self.guestTotal = 0
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot]{
                for snap in snapshots{
                    if let postDictionary = snap.value as? Dictionary<String, AnyObject>{
                        //print(postDictionary)
                        
                        let filerStatusOne = (snap.value! as AnyObject).object(forKey: "assignGuestToParty") as! String
                        let filerStatusTwo = (snap.value! as AnyObject).object(forKey: "guestRSVPStatus") as! String
                        //print(filerStatusOne )
                        //print(filerStatusTwo )
                        
                        let guestFamilySize = (snap.value! as AnyObject).object(forKey: "guestFamilySize") as! Int
                        
                         if self.filterBtn.titleLabel?.text == "Groom's Guest"{
                            if filerStatusOne == "Groom's Guest"{
                                let key = snap.key
                                let guest = GuestProfile(key: key , dictionary: postDictionary)
                                // Items are returned chronologically, but here the newest guest is first.
                                
                                self.guests.insert(guest , at: 0)
                                self.guestTotal += guestFamilySize
                            }
                        }else if self.filterBtn.titleLabel?.text == "Bride's Guest"{
                            if filerStatusOne == "Bride's Guest"{
                                let key = snap.key
                                let guest = GuestProfile(key: key , dictionary: postDictionary)
                                // Items are returned chronologically, but here the newest guest is first.
                                
                                self.guests.insert(guest , at: 0)
                                self.guestTotal += guestFamilySize
                            }
                        }else if self.filterBtn.titleLabel?.text == "Accept Invitation"{
                            if filerStatusTwo == "Accept"{
                                let key = snap.key
                                let guest = GuestProfile(key: key , dictionary: postDictionary)
                                // Items are returned chronologically, but here the newest guest is first.
                                
                                self.guests.insert(guest , at: 0)
                                self.guestTotal += guestFamilySize
                            }
                            
                        }else if self.filterBtn.titleLabel?.text == "Regret Invitation"{
                            if filerStatusTwo == "Regret"{
                                let key = snap.key
                                let guest = GuestProfile(key: key , dictionary: postDictionary)
                                // Items are returned chronologically, but here the newest guest is first.
                                
                                self.guests.insert(guest , at: 0)
                                self.guestTotal += guestFamilySize
                            }
                            
                        }else{
                            // Here self.filterBtn.titleLabel?.text == "All Guest"
                            let key = snap.key
                            let guest = GuestProfile(key: key , dictionary: postDictionary)
                            // Items are returned chronologically, but here the newest guest is first.
                            
                            self.guests.insert(guest , at: 0)
                            self.guestTotal += guestFamilySize
                        }
                    }
                }
            }
            // Be sure that the tableView updates when there is new data.
            self.guestTableView.reloadData()
            self.totalGuestCnt.text = String(self.guestTotal)
            SwiftLoader.hide()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.guestTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func FilterGuestList(_ sender: UIButton) {
        self.view.addSubview(self.guestPicker)

    }
    
    @IBAction func menuAction(_ sender: AnyObject) {
        self.slideMenuController()?.openLeft()
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return guests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuestCell", for: indexPath) as! GuestListTableViewCell
        let guest = guests[(indexPath as NSIndexPath).row]
        
        cell.configureCell(guest)
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let guestKey = self.guests[(indexPath as NSIndexPath).row].guestKey
        let delete = UITableViewRowAction(style: UITableViewRowActionStyle.default , title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            let alert_DeleteGuest = UIAlertController(title: "Warning", message: "Are you sure you want to delete this guest?", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction) -> Void in
                SwiftLoader.show(animated: true)
                //observeSingleEventOfType reads data once. This is different then other listeners.
                NetworkManager.sharedInstance.GUEST_REF.observeSingleEvent(of: .value, with: { snapshot in
                    //print(snapshot.value)
                    for item in (snapshot.children.allObjects as? [FIRDataSnapshot])! {
                        if item.key as String == guestKey{
                            NetworkManager.sharedInstance.GUEST_REF.child(item.key).removeValue()
                        }
                    }
                    SwiftLoader.hide()
                    }, withCancel: { (error) -> Void in
                        print(error.localizedDescription)
                        SwiftLoader.hide()
                })
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alert_DeleteGuest.addAction(okAction)
            alert_DeleteGuest.addAction(cancelAction)
            self.present(alert_DeleteGuest, animated: true, completion: nil)
        }
        
        let edit = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Edit") { (action, indexPath) in
            // Edit item at indexPath
            let storyboard:UIStoryboard = UIStoryboard(name: "GuestList", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "UpdateGuestVC") as? UpdateGuestInformationVC
                else{return}
            vc.name = self.guests[(indexPath as NSIndexPath).row].guestName
            vc.street = self.guests[(indexPath as NSIndexPath).row].guestAddress_Street
            vc.city = self.guests[(indexPath as NSIndexPath).row].guestAddress_City
            vc.state = self.guests[(indexPath as NSIndexPath).row].guestAddress_State
            vc.zipcode = self.guests[(indexPath as NSIndexPath).row].guestAddress_ZipCode
            vc.emailString = self.guests[(indexPath as NSIndexPath).row].guestEmailAddress
            vc.guestCnt = self.guests[(indexPath as NSIndexPath).row].guestFamilySize
            vc.phone = self.guests[(indexPath as NSIndexPath).row].guestPhoneNumber
            vc.partyStatus = self.guests[(indexPath as NSIndexPath).row].assignGuestToParty
            vc.rsvpStatus = self.guests[(indexPath as NSIndexPath).row].guestRSVPStatus
            vc.key = self.guests[(indexPath as NSIndexPath).row].guestKey
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
        edit.backgroundColor = UIColor.blue
        
        return [delete, edit]
    }
    
    func configurePicker(){
        self.guestPicker.dataSource = self
        self.guestPicker.frame = CGRect(x: 0, y: 25, width: self.view.frame.width, height: 100)
        self.guestPicker.backgroundColor = iAdornConstants.fushiaColor
        self.guestPicker.tintColor = UIColor.white
    }
    
    //MARK: UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return pickerItems.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerItems[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //print(pickerItems[row] as? String)
        
        let result = pickerItems[row] as? String
        if result == "Export 'All Guests'"{
            SwiftLoader.show(animated: true)
            filterBtn.setTitle("All Guests", for: UIControlState())
            self.viewDidLoad()
            
            NetworkManager.sharedInstance.CURRENT_USER_REF.observe(.value, with: { snapshot in
                //print(snapshot.value)
                let firstname = (snapshot.value! as AnyObject).object(forKey: "username") as! String
                    //snapshot.value!["username"]!!.description
                let spouse = (snapshot.value! as AnyObject).object(forKey: "spousename") as! String
                    //snapshot.value!["spousename"]!!.description
                let userName = firstname as String
                let spouseName = spouse as String
                
                let titlemMessage = "iAdorn Wedding Planner\n\n"
                
                var guestData = ""
                
                if spouseName != ""{
                    guestData = "Check out \(userName) and \(spouseName)'s Guest List!\nTotal Guest Attending: \(self.guestTotal)\n\n"
                }else{
                    guestData = "Check out \(userName)'s Guest List!\nTotal Guest Attending: \(self.guestTotal)\n\n"
                }
                var num = 0
                for item in self.guests{
                    num += 1
                    
                    var street = String(item.guestAddress_Street)
                    if  street != ""{
                        street = "\n\(street)"
                    }else{
                        street = ""
                    }
                    
                    var state = String(item.guestAddress_State)
                    if  state != ""{
                        state = "\n\(item.guestAddress_State), \(item.guestAddress_ZipCode) "
                    }else{
                        state = ""
                    }
                    
                    let guestInfo = "\(num)) \(item.guestName)\nAddress:\(street) \(state) \nEmail: \(item.guestEmailAddress) \nPhone: \(item.guestPhoneNumber) \nRSVP: \(item.guestRSVPStatus) \nParty: \(item.assignGuestToParty) \nAttending: \(item.guestFamilySize) \n--------------------"
                    guestData += guestInfo + "\n\n"
                }
                
                let objectsToShare = [titlemMessage, guestData]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = pickerView
                self.present(activityVC, animated: true, completion: nil)
                }, withCancel: { (error) -> Void in
                    print(error.localizedDescription)
                    showAlertWithOKButton("Alert", message: "Unable to export Guest List at this time.", presentingViewController: self)
            })
            SwiftLoader.hide()
            
        }else{
            filterBtn.setTitle(result, for: UIControlState())
        }
        guestPicker.removeFromSuperview()
        viewDidLoad()
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
            let attributedString = NSAttributedString(string: pickerItems[row] as! String, attributes: [NSForegroundColorAttributeName : UIColor.white])
            return attributedString
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

