//
//  iAdornConstants.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/16/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit

class iAdornConstants: NSObject {

    static let fushiaColor : UIColor =  UIColor(red: 199/255.0, green: 28/255.0, blue: 124/255.0, alpha: 1)
    static let pinterestAppId = "4838684475223197262"
    
    //check list //
    static let checkList_headerTitles = ["Custom List", "12 - 10 Months Before", "9 - 7 Months Before", "6 - 4 Months Before", "3 - 1 Months Before", "2 Weeks Before"]
    
    static var checkList: [[String]] = [
        
        //Custom
        [
            "Add Item"
        ],
        
        //12 - 10 Months
        [
            "Announce engagement",
            "Decide on wedding budget",
            "Hire a wedding planner",
            "Decide on wedding party",
            "Choose wedding style",
            "Decide on date and time",
            "Select ceremony location",
            "Select reception location",
            "Research vendors",
            "Begin guest list",
            "Explore wedding gown",
            "Explore Groom's attire",
            "Create a binder to organize your thoughts",
            "Plan engagement party",
            "schedule pre-marital counseling"
        ],
        
        //9 - 7 Months
        [
            "Decide on wedding colors",
            "Purchase a dress",
            "Choose bridesmaid's dresses",
            "Research a florist",
            "Book your officiant",
            "Book your photographer",
            "Book your videographer",
            "Book your DJ or band",
            "Meet caterers",
            "Arrange accommodations",
            "Decide on wedding cake",
            "Begin wedding registry",
            "Finalize guest list",
            "Collect guest addresses",
            "Research wedding stationary",
            "Research honeymoon options"
        ],
        
        //6 - 4 Months
        [
            "Book a florist",
            "Select and purchase invitations",
            "Select tuxedos",
            "Send save-the-dates",
            "Reserve a block of hotel rooms for out-of-town guests",
            "Appraise engagment ring",
            "Shop for wedding bands",
            "Create wedding website",
            "Send engagement announcement to newspapers",
            "Discuss wedding vows",
            "Book honeymoon",
            "Compose day-of timeline",
            "Book rehearsal",
            "Book rehearsal dinner venue",
            "Purchase wedding accessories",
            "Book hair and makeup artist"
        ],
        
        //3 - 1 Months
        [
            "Finalize flowers",
            "Book rental items",
            "Discuss menu options",
            "Arrange limousine service",
            "Shop for bridal party gifts",
            "Send out invitations",
            "Select ceremony music",
            "Schedule alterations",
            "Purchase brides accessories",
            "Apply for marriage license",
            "Finalize honeymoon itinerary",
            "Enjoy bachelorette party",
            "Send out final payments",
            "Pick up your dress",
            "Book a spa day"
        ],
        
        //2 weeks before
        [
            "Finalize all accessories, shoes and lingerie",
            "Schedule hair and makeup trial",
            "Finalize programs",
            "Finalize menu cards",
            "Finalize seating chart",
            "send rehearsal invitations",
            "Confirm all transportation plans",
            "Break in wedding shoes",
            "Give a final guest count to the caterer",
            "Confirm honeymoon arrangements",
            "Pack for the honeymoon",
            "Schedule mani/pedi appointment",
            "Update registries",
            "Make arrangements for any pets",
            "Get final haircut and color",
            "Delegate wedding day duties",
            "Prepare wedding day emergency kit"
        ]
    
        ];
}

func showAlertWithOKButton(_ title: String, message: String, presentingViewController: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
    alert.addAction(action)
    presentingViewController.present(alert, animated: true, completion: nil)
}
