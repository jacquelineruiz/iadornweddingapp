//
//  LeftMenuCell.swift
//  iAdorn
//
//  Created by Jaxs on 4/20/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {

    @IBOutlet weak var selectedIndicatorView: UIView!
    @IBOutlet weak var cellLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
