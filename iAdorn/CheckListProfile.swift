//
//  CheckListItem.swift
//  iAdorn
//
//  Created by Jaxs on 5/24/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class CheckListProfile: NSObject {

    fileprivate var _listItemRef: FIRDatabaseReference!
    fileprivate var _completed: Bool!
    fileprivate var _item: String!
    fileprivate var _section: String!
    fileprivate var _itemKey: String!
    
    var completed: Bool {
        return _completed
    }
    
    var item: String {
        return _item
    }
    
    var section: String {
        return _section
    }
    
    var itemKey: NSString {
        return _itemKey! as NSString
    }
    
    // Initialize the new check list item
    init(list:String, key: String, dictionary: Dictionary<String, AnyObject>) {
        self._itemKey = key
        self._section = list

        // Within the item, or Key, the following properties are children
        if let completed  = dictionary["completed"] as? Bool {
            self._completed = completed
        } else {
            self._completed  = false
        }
        
        if let item = dictionary["item"] as? NSString {
            self._item = item as String!
        }else{
            self._item = "Add Item"
        }
        
        self._listItemRef = NetworkManager.sharedInstance.CHECK_LIST_REF.child("\(list)/\(self._itemKey!)")
    }
    
    
    func updatCompletedStatus(_ update: Bool) {
        
         if update {
            _completed = true
         } else {
            _completed = false
         }
        
        let newValue = ["completed": _completed as AnyObject]
        // Save the update in Firebase
        self._listItemRef!.updateChildValues(newValue)
    }
}
