//
//  LoginVC.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/16/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import SwiftLoader
import Firebase
import FirebaseAuth

class LoginVC: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var iAdornMessage: UILabel!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.email.delegate = self
        self.password.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification:Notification) {
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        bottomLayoutConstraint.constant = keyboardHeight + 10.0
        self.updateView()
    }
    
    func keyboardWillHide(_ notification:Notification) {
        bottomLayoutConstraint.constant = 122.0
        self.updateView()
    }
    
    fileprivate func updateView() {
        UIView.animate(withDuration: 0.24, animations: {
            self.iAdornMessage.alpha = self.bottomLayoutConstraint.constant == 122.0 ? 1:0
            self.view.layoutIfNeeded()
        }) 
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: UIButton) {
        SwiftLoader.show(animated: true)
        let email = self.email.text
        let password = self.password.text
        
        if email != "" && password != "" {
            // Login with the Firebase's authUser method
            FIRAuth.auth()?.signIn(withEmail: email!, password: password!, completion: {
                (user, error) in
                
                if error != nil {
                    showAlertWithOKButton("Error", message: "\(error!.localizedDescription)", presentingViewController: self)
                } else {
                    // Be sure the correct uid is stored.
                    UserDefaults.standard.setValue(user!.uid, forKey: "uid")
                    
                    // Enter the app!
                    let storyboard:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                    guard let vc = storyboard.instantiateViewController(withIdentifier: "ContainerSlideMenu") as? ContainerSlideMenu
                        else{return}
                    vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    self.present(vc, animated: true, completion: nil)
                    self.password.text = ""
                }
                SwiftLoader.hide()
            })
        } else {
            
            // There was a problem
            SwiftLoader.hide()
            showAlertWithOKButton("Oops!", message: "Don't forget to enter your email and password.", presentingViewController: self)
        }
        
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
       
        let alert = UIAlertController(title: "Forgot Password?", message: "Please Enter Email Address", preferredStyle: .alert)
        alert.addTextField {
            (textField: UITextField!) -> Void in
            
            if self.email.text != ""{
                textField.text = self.email.text
            }else{
                textField.text = ""
            }
        }
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let textField = alert.textFields![0]
            if textField.text != ""{
                
                FIRAuth.auth()?.sendPasswordReset(withEmail: textField.text!, completion: { (error) in
                    if error != nil {
                         showAlertWithOKButton("Alert", message: "\(error!.localizedDescription)", presentingViewController: self)
                    } else {
                          showAlertWithOKButton("", message: "Password reset sent successfully. \nPlease check your email.", presentingViewController: self)
                    }
                })
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
