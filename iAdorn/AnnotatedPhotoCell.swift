//
//  AnnotatedPhotoCell.swift
//  RWDevCon
//
//  Created by Mic Pringle on 26/02/2015.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import PinterestSDK

class AnnotatedPhotoCell: UICollectionViewCell {
  
  @IBOutlet weak var pdkImage: UIImageView!
  @IBOutlet weak var imageViewHeightLayoutConstraint: NSLayoutConstraint!
  @IBOutlet weak var captionLabel: UILabel!
  @IBOutlet weak var commentLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        pdkImage.image = nil
    }

}
