//
//  SignupVC.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/16/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import SwiftLoader
import FirebaseAuth

class SignupVC: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var iAdornMessage: UILabel!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.email.delegate = self
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.phoneNumber.delegate = self
        self.password.delegate = self
        self.confirmPassword.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(_ notification:Notification) {
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        bottomLayoutConstraint.constant = keyboardHeight + 10.0
        self.updateView()
    }
    
    func keyboardWillHide(_ notification:Notification) {
        bottomLayoutConstraint.constant = 94.0
        self.updateView()
    }
    
    fileprivate func updateView() {
        UIView.animate(withDuration: 0.24, animations: {
            self.iAdornMessage.alpha = self.bottomLayoutConstraint.constant == 94.0 ? 1:0
            self.view.layoutIfNeeded()
        }) 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signup(_ sender: UIButton) {
        if self.password.text != self.confirmPassword.text{
            showAlertWithOKButton("Ops", message:  "Passwords do not match", presentingViewController: self)
        }else{
            
            let firstName = self.firstName.text
            let lastName = self.lastName.text
            let email = self.email.text
            let password = self.password.text
            let phoneNumber = self.phoneNumber.text
            
            if firstName != "" && lastName != "" && email != "" && password != "" && phoneNumber != ""{
                
                // Set Email and Password for the New User.
                SwiftLoader.show(animated: true)
                
                FIRAuth.auth()?.createUser(withEmail: email!, password: password!)  { (user, error) in
                    
                    if error != nil {
                        SwiftLoader.hide()
                        showAlertWithOKButton("Error", message: "\(error!.localizedDescription)", presentingViewController: self)
                    } else {
                        if let user = FIRAuth.auth()?.currentUser {
                            let userInfo = [ "email": email!, "displayName": firstName!, "username": firstName!, "lastname": lastName!, "phonenumber": phoneNumber!]
                            
                            let uid = user.uid;  // The user's ID, unique to the Firebase project.
                            // Store the uid for future access
                            UserDefaults.standard.setValue(uid, forKey: "uid")
                            
                            NetworkManager.sharedInstance.updateUser(userInfo as Dictionary<String, AnyObject>)
                            NetworkManager.sharedInstance.initializeCheckList()
                            
                            let date = Date()
                            // create a new feed for the home scene.
                            let newHomeFeed: Dictionary<String, AnyObject> = [
                                "_feedMessage": "Congratulations! Your account was successfully created." as AnyObject,
                                "_feedDate": date.formatAsStringWithTime as AnyObject
                            ]
                            // Send it over to DataService to seal the deal.
                            //print(newHomeFeed)
                            NetworkManager.sharedInstance.createHomeFeed(newHomeFeed)
                            
                            // Enter the app.
                            let storyboard:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "ContainerSlideMenu") as? ContainerSlideMenu
                                else{return}
                            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.navigationController?.pushViewController(vc, animated: true)
                            SwiftLoader.hide()
                        }
                    }
                }
                
                
            } else {
                showAlertWithOKButton("Oops!", message: "Don't forget to enter text in all fields.", presentingViewController: self)
            }
        }
        
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
