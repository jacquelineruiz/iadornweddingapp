//
//  Date.swift
//  iAdorn
//
//  Created by Jaxs on 6/25/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import Foundation
import UIKit

extension Date{
    /// Returns the amount of days from another date
    func daysFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.day, from: date, to: self, options: []).day!
    }
    
    var formatAsString:String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter.string(from: self)
    }
    
    var formatAsStringWithTime:String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
}
