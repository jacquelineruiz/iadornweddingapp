//
//  HomeFeedTableViewCell.swift
//  iAdorn
//
//  Created by Jaxs on 6/26/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit

class HomeFeedTableViewCell: UITableViewCell {

    
    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var feedMessage: UILabel!
    @IBOutlet weak var feedDate: UILabel!
    var homeFeed: HomeFeedProfile!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*
         Note: clipsToBounds causes UIImage to not display in iOS10 & Xcode 8
         http://stackoverflow.com/q/39380128/4793221
        */
        
        self.layoutIfNeeded()
        feedImage.layer.cornerRadius = self.feedImage.frame.size.height/2
        feedImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(_ feed: HomeFeedProfile) {
        NetworkManager.sharedInstance.CURRENT_USER_REF.observe(.value, with: { snapshot in
            
            let userImage = (snapshot.value! as AnyObject).object(forKey: "userImage") as! String
            let image = userImage
            
            if image.isEmpty{
                
                return
            }else{
                let image = userImage
                let decodedData = Data(base64Encoded: image, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                self.feedImage.image = UIImage(data: decodedData!)
            }
            
            self.homeFeed = feed
            self.feedMessage.text = "\(feed.feedMessage)"
            self.feedDate.text = "\(feed.feedDate)"
            }, withCancel: { (error) -> Void in
                print(error.localizedDescription)
        })
    }
    

}
