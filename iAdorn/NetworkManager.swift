//
//  NetworkManager.swift
//  iAdorn
//
//  Created by Jacqueline Caraballo on 3/16/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class NetworkManager: NSObject {
    
    var rootRef = FIRDatabase.database().reference()
    
    class var sharedInstance: NetworkManager {
        struct Singleton {
            static let instance = NetworkManager()
        }
        return Singleton.instance
    }
    
    var USER_REF: String {
        let FireBaseUrl_UserRef = "\(rootRef)/users"
        return FireBaseUrl_UserRef
    }
    
    var CHECK_LIST_REF: FIRDatabaseReference{
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let FireBaseUrl_CheckListRef = rootRef.child("users/\(userID)/checklist")
        
        return FireBaseUrl_CheckListRef
    }
    
    var GUEST_REF: FIRDatabaseReference{
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let FireBaseUrl_GuestRef = rootRef.child("users/\(userID)/guests")
        
        return FireBaseUrl_GuestRef
    }
    
    var HOME_FEED_REF: FIRDatabaseReference{
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let FireBaseUrl_HomeFeedRef = rootRef.child("users/\(userID)/home_feed")
        
        return FireBaseUrl_HomeFeedRef
    }
    
    var CURRENT_USER_REF: FIRDatabaseReference {
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
         let currentUser = rootRef.child("users/\(userID)")
        
        return currentUser
    }
    
    var CURRENT_USER_HOME_FEED: FIRDatabaseReference {
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let currentUser = rootRef.child("users/\(userID)/home_feed")
        
        return currentUser
    }
    
    var Current_User_Guests: FIRDatabaseReference{
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let currentGuests = rootRef.child("users/\(userID)/guests")
        
        return currentGuests
    }
    
    func createNewAccount(_ user: Dictionary<String, String>) {
        // A User is born.
        // childByAutoId() saves the user and gives it its own ID.
        // setValue() saves to Firebase.
        rootRef.child("users").childByAutoId().setValue(user)
    }
    
    
    func createNewGuest(_ guest: Dictionary<String, AnyObject>) {
        // A guest is born.
        // childByAutoId() saves the guest and gives it its own ID.
        // setValue() saves to Firebase.
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        rootRef.child("users/\(userID)/guests").childByAutoId().setValue(guest)
    }
    
    func createHomeFeed(_ homeFeed: Dictionary<String, AnyObject>) {
        // feed is created.
        // childByAutoId() saves the guest and gives it its own ID.
        // setValue() saves to Firebase.
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        rootRef.child("users/\(userID)/home_feed").childByAutoId().setValue(homeFeed)
    }
    
    func updateUser(_ userUpdate: Dictionary<String, AnyObject>){
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let currentUser = rootRef.child("users/\(userID)")
        currentUser.updateChildValues(userUpdate)
    }
    
    func initializeCheckList(){
        NetworkManager.sharedInstance.CURRENT_USER_REF.observe(.value, with: {
            snapshot in
            
            //IF USER DOES NOT HAVE CHECKLIST ITEMS IN FIREBASE SERVER
            if (!snapshot.childSnapshot(forPath: "checklist").exists()) {
              print("checklist doesnt exist")
            
                for item in iAdornConstants.checkList[0] {
                    let items: Dictionary<String, AnyObject> = [
                        "item": item as AnyObject,
                        "completed": false as AnyObject
                    ]
                    NetworkManager.sharedInstance.CHECK_LIST_REF.child("customList").childByAutoId().setValue(items, withCompletionBlock: { (error:NSError?, ref: FIRDatabaseReference) in
                        if (error != nil){
                            print(error?.localizedDescription)
                        }
                        
                    } as! (Error?, FIRDatabaseReference) -> Void)
                }
                for item in iAdornConstants.checkList[1] {
                    let items: Dictionary<String, AnyObject> = [
                        "item": item as AnyObject,
                        "completed": false as AnyObject
                    ]
                    NetworkManager.sharedInstance.CHECK_LIST_REF.child("12MonthList").childByAutoId().setValue(items, withCompletionBlock: { (error:NSError?, ref: FIRDatabaseReference) in
                        if (error != nil){
                            print(error?.localizedDescription)
                        }
                        
                    } as! (Error?, FIRDatabaseReference) -> Void)
                }

                for item in iAdornConstants.checkList[2] {
                    let items: Dictionary<String, AnyObject> = [
                        "item": item as AnyObject,
                        "completed": false as AnyObject
                    ]
                    NetworkManager.sharedInstance.CHECK_LIST_REF.child("9MonthList").childByAutoId().setValue(items, withCompletionBlock: { (error:NSError?, ref: FIRDatabaseReference) in
                        if (error != nil){
                            print(error?.localizedDescription)
                        }
                        
                    } as! (Error?, FIRDatabaseReference) -> Void)
                }
                
                for item in iAdornConstants.checkList[3] {
                    let items: Dictionary<String, AnyObject> = [
                        "item": item as AnyObject,
                        "completed": false as AnyObject
                    ]
                    NetworkManager.sharedInstance.CHECK_LIST_REF.child("6MonthList").childByAutoId().setValue(items, withCompletionBlock: { (error:NSError?, ref: FIRDatabaseReference) in
                        if (error != nil){
                            print(error?.localizedDescription)
                        }
                        
                    } as! (Error?, FIRDatabaseReference) -> Void)
                }

                
                for item in iAdornConstants.checkList[4] {
                    let items: Dictionary<String, AnyObject> = [
                        "item": item as AnyObject,
                        "completed": false as AnyObject
                    ]
                    NetworkManager.sharedInstance.CHECK_LIST_REF.child("3MonthList").childByAutoId().setValue(items, withCompletionBlock: { (error:NSError?, ref: FIRDatabaseReference) in
                        if (error != nil){
                            print(error?.localizedDescription)
                        }
                        
                    } as! (Error?, FIRDatabaseReference) -> Void)
                }
                
                for item in iAdornConstants.checkList[5] {
                    let items: Dictionary<String, AnyObject> = [
                        "item": item as AnyObject,
                        "completed": false as AnyObject
                    ]
                    NetworkManager.sharedInstance.CHECK_LIST_REF.child("2WeekList").childByAutoId().setValue(items, withCompletionBlock: { (error:NSError?, ref: FIRDatabaseReference) in
                        if (error != nil){
                            print(error?.localizedDescription)
                        }
                        
                    } as! (Error?, FIRDatabaseReference) -> Void)
                }
            
            }else{
                print("checklist does exist")
            }
        })
    }
    
}
