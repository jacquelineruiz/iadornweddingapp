//
//  HomeFeedProfile.swift
//  iAdorn
//
//  Created by Jaxs on 6/26/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class HomeFeedProfile: NSObject {

    
    fileprivate var _feedRef: FIRDatabaseReference!
    fileprivate var _feedMessage: String!
    fileprivate var _feedDate: String!
    fileprivate var _feedKey: String!
    
    var feedMessage: String {
        return _feedMessage
    }
    
    var feedDate: String {
        return _feedDate
    }
    
    var feedKey: String {
        return _feedKey
    }

    
    
    // Initialize the new Guest
    init(key: String, dictionary: Dictionary<String, AnyObject>) {
        self._feedKey = key
        
        // Within the Key, the following properties are children
        
        if let feed_message = dictionary["_feedMessage"] as? String {
            self._feedMessage = feed_message
        }
        
        if let feed_Date = dictionary["_feedDate"] as? String {
            self._feedDate = feed_Date
        }
        
        self._feedRef = NetworkManager.sharedInstance.HOME_FEED_REF.child(self._feedKey)
        
    }

    
    
}
