//
//  ContainerSlideMenu.swift
//  iAdorn
//
//  Created by Jaxs on 4/20/16.
//  Copyright © 2016 Jacqueline Ruiz. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ContainerSlideMenu: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func awakeFromNib() {
        let storyboardMain:UIStoryboard = UIStoryboard(name: "Home", bundle: nil)

        let main  = storyboardMain.instantiateViewController(withIdentifier: "Home")
            self.mainViewController = main
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LeftMenu") {
            self.leftViewController = controller
        }
        super.awakeFromNib()
    }

}
